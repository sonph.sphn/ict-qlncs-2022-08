﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Erp.Infrastructures;
using App.Erp.Providers;
using Core.Domain.Organization;
using Core.Domain.System;
using Core.Helper;
using Core.Services.Security;
using Core.Services.System;

namespace App.Erp.Context
{
    public static class WorkContext
    {
        public static SystemUser User
        {
            get
            {
                var u = (SystemUser)HttpContext.Current.Items[Core.Constant.Security.CURRENT_USER_KEY];
                if (u != null) return u;
                u = DependencyRegistrar.Get<ISessionService>().GetUser(HttpContext.Current.User.Identity.Name);
                if (u == null)
                {
                    DependencyRegistrar.Get<ISignInService>().SignOut();
                    HttpContext.Current.Response.Redirect("/Account/Login");
                }
                HttpContext.Current.Items[Core.Constant.Security.CURRENT_USER_KEY] = u;
                return u;
            }
        }

        public static SystemUser StoredUser
        {
            get
            {
                var u = (SystemUser)HttpContext.Current.Items[Core.Constant.Security.CURRENT_USER_KEY];
                if (u != null) return u;
                u = DependencyRegistrar.Get<ISessionService>().GetUser(HttpContext.Current.User.Identity.Name);
                return u;
            }
        }

        public static List<SystemFunction> SystemFuncs
        {
            get
            {
                var fs = (List<SystemFunction>)HttpContext.Current.Items["SYSTEM_FUNCTIONS"];
                if (fs != null) return fs;
                fs = DependencyRegistrar.Get<IFunctionService>().Gets().ToList();
                HttpContext.Current.Items["SYSTEM_FUNCTIONS"] = fs;
                return fs;
            }
        }

        public static List<SystemFunction> Functions
        {
            get
            {
                if (User == null) return new List<SystemFunction>();
                var fs = (List<SystemFunction>)HttpContext.Current.Items[Core.Constant.Security.FUNCTIONS];
                if (fs != null) return fs;
                if (User.IsAdmin)
                {
                    fs = SystemFuncs;
                }
                else
                {
                    var fIds = DependencyRegistrar.Get<IUserService>().GetFunctionIds(User.Id);
                    fs = SystemFuncs.Where(i => !i.IsAdmin && fIds.Contains(i.Id)).ToList();
                }
                HttpContext.Current.Items[Core.Constant.Security.FUNCTIONS] = fs;
                return fs;
            }
        }

        public static string GetMenuModel()
        {
            var cats = DependencyRegistrar.Get<IFunctionCategoryService>().Gets().OrderBy(i => i.Order).Select(i => new { i.Id, i.Name, i.Icon }).ToList();
            var r = new List<object>();
            foreach (var c in cats)
            {
                r.Add(new
                {
                    c.Name,
                    c.Icon,
                    Functions = Functions.Where(i => i.CategoryId == c.Id).OrderBy(i => i.Order).Select(i => new { i.Url, i.Name, i.Icon }).ToList()
                });
            }
            return r.ToJson();
        }

        public static void ClearCache()
        {
            HttpContext.Current.Items.Clear();
            SettingProvider.ClearCache();
        }
    }
}