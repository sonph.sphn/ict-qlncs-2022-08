﻿using System.Collections.Generic;
using Core.Domain.System;
using Core.Services.Base;

namespace App.Erp.Context
{
    public class WorkContextService : IWorkContextService
    {
        public int GetCurrentUserId()
        {
            return WorkContext.StoredUser?.Id ?? 0;
        }

        public SystemUser GetCurrentUser()
        {
            return WorkContext.StoredUser;
        }

        public List<SystemFunction> GetCurrentUserFunction()
        {
            return WorkContext.Functions;
        }
    }
}