﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Erp.Context;

namespace App.Erp.Security
{
    public class FunctionAttribute : AuthorizeAttribute
    {
        private readonly string[] _accessKeys;
        public FunctionAttribute(params string[] keys)
        {
            _accessKeys = keys;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
            {
                return false;
            }
            if (WorkContext.User.IsAdmin)
            {
                return true;
            }
            return WorkContext.Functions.Any(i => _accessKeys.Any(j => j == i.Key));
        }
    }
}
