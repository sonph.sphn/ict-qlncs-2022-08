﻿using System.Web;
using System.Web.Mvc;
using App.Erp.Context;

namespace App.Erp.Security
{
    public class AdminAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            return isAuthorized && WorkContext.User.IsAdmin;
        }
    }
}
