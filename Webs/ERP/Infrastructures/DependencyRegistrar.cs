﻿using System.Data.Entity;
using System.Reflection;
using App.Erp.Context;
using App.Erp.Providers;
using Autofac;
using Autofac.Integration.Mvc;
using Core.Data.Base;
using Core.Data.Mapping;
using Core.Providers;
using Core.Services.Base;
using Core.Services.Organize;
using Core.Services.Security;
using Core.Services.System;

namespace App.Erp.Infrastructures
{
    public static class DependencyRegistrar
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetCallingAssembly());
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<ErpDataContext>().As<DbContext>().InstancePerDependency();
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IBaseRepository<>)).InstancePerDependency();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerDependency();
            builder.RegisterType<SessionService>().As<ISessionService>().InstancePerDependency();
            builder.RegisterType<ProvinceService>().As<IProvinceService>().InstancePerDependency();
            builder.RegisterType<RoleService>().As<IRoleService>().InstancePerDependency();
            builder.RegisterType<FunctionService>().As<IFunctionService>().InstancePerDependency();
            builder.RegisterType<FunctionCategoryService>().As<IFunctionCategoryService>().InstancePerDependency();
            builder.RegisterType<SignInService>().As<ISignInService>().InstancePerDependency();
            builder.RegisterType<BaseAuthenticationService>().As<IAuthenticationService>().InstancePerDependency();
            builder.RegisterType<SettingService>().As<ISettingService>().InstancePerDependency();
            builder.RegisterType<RoleFunctionService>().As<IRoleFunctionService>().InstancePerDependency();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>().InstancePerDependency();
            builder.RegisterType<SettingGroupService>().As<ISettingGroupService>().InstancePerDependency();
            builder.RegisterType<BaseSettingProvider>().As<ISettingProvider>().InstancePerDependency();
            builder.RegisterType<WorkContextService>().As<IWorkContextService>().InstancePerDependency();
            builder.RegisterType<BaseSettingProvider>().As<ISettingProvider>().InstancePerDependency();
            builder.RegisterType<Xtt2020Service>().As<IXtt2020Service>().InstancePerDependency();
            builder.RegisterType<CareerService>().As<ICareerService>().InstancePerDependency();

            Container = builder.Build();
        }
        public static T Get<T>() => Container.Resolve<T>();
        public static T Get<T>(string keyName) => Container.ResolveKeyed<T>(keyName);
        public static IContainer Container { get; private set; }
    }
}