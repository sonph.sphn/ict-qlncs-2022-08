﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Erp;

namespace App.Erp.Models
{
    public class Chuyen2020
    {
        public int Id { set; get; }
        public string FullName { set; get; }
        public DateTime Birthday { set; get; }
        public string HomeTown { set; get; }
        public string Province { set; get; }
        public string M1 { set; get; }
        public string M2 { set; get; }
        public string M3 { set; get; }
        public string M4 { set; get; }
        public string Total { set; get; }
        public int Result { set; get; }
    }
}