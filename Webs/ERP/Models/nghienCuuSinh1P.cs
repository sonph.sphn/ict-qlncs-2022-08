﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Erp.Models
{
    public class nghienCuuSinh1P
    {
		public long Id { get; set; }

		public string HoTen { get; set; }

		public string GioiTinh { get; set; }

		public DateTime? NgaySinh { get; set; }

		public string SoDienThoai { get; set; }

		public string Email { get; set; }

		public string NoiSinh { get; set; }

		public string DiaChiLienLac { get; set; }

		public string NgheNghiep { get; set; }

		public string CoQuanCongTacHienNay { get; set; }

		public string NamBatDauCongTac { get; set; }

		public string HienLaCanBo { get; set; }

		public string ViTriConViecHienTai { get; set; }

		public string ThamNiemNghieNghiep { get; set; }

		public string ChuyenMon { get; set; }

		public string Truong_TN_DaiHoc { get; set; }

		public int? Nam_TN_DaiHoc { get; set; }

		public string HeDaoTao_DaiHoc { get; set; }

		public string Nghanh_TN_DaiHoc { get; set; }

		public string DiemTrungBinh_DaiHoc { get; set; }

		public string LoaiTotNghiep_DaiHoc { get; set; }

		public string Url_FileUpload_DaiHoc { get; set; }

		public string Truong_TN_ThacSi { get; set; }

		public string Nam_TN_ThacSi { get; set; }

		public string HeDaoTao_ThacSi { get; set; }

		public string Nghanh_TN_ThacSi { get; set; }

		public string DiemTrungBinh_ThacSi { get; set; }

		public string Url_FileUpload_ThacSi { get; set; }

		public string NgoaiNgu { get; set; }

		public string LoaiVanBangNgoaiNgu { get; set; }

		public string Url_ChungChiNgoaiNgu { get; set; }

		public string BoTucKienThuc { get; set; }

		public int ChuyenNghanhDuTuyenId { get; set; }

		public string TenChuyenNghanhDuTuyen { get; set; }

		public string DoiTuongDuTuyen { get; set; }

		public string ThoiGianHinhThucDaoTao { get; set; }

		public int? Status { get; set; }

		public string TenDeTai { get; set; }

		public int? KhoaId_NHD1 { get; set; }

		public int? Id_NHD1 { get; set; }

		public string NHD1 { get; set; }

		public int? KhoaId_NHD2 { get; set; }

		public int? Id_NHD2 { get; set; }

		public string NHD2 { get; set; }

		public string CoQuanCongTac_NHD2 { get; set; }

		public string NoiDungPhanHoi { get; set; }

		public string TepFilePhanHoi { get; set; }

		public string MaNCS { get; set; }

		public string Truong_TN_VB2 { get; set; }

		public int? Nam_TN_VB2 { get; set; }

		public string HeDaoTao_VB2 { get; set; }

		public string Nganh_TN_VB2 { get; set; }

		public string LoaiTotNghiep_VB2 { get; set; }

		public string Url_FileUpload_VB2 { get; set; }

		public string DiemTrungBinh_VB2 { get; set; }

		public DateTime? CreatedAt { get; set; }

		public DateTime? UpdatedAt { get; set; }

		public string CreatedBy { get; set; }
		
		public string UpdatedBy { get; set; }

		public bool? CapQuyenTruyCap { get; set; }

		public string SoCMND { get; set; }

		public DateTime? Ngaycap_CMND { get; set; }

		public string Noicap_CMND { get; set; }

		public string TinhThanh_CMND { get; set; }

		public string Quan_CMND { get; set; }

		public string Xa_CMND { get; set; }

		public string Url_FileUpload_AnhSoYeuLyLich { get; set; }

		public string Url_FileUpload_SoYeuLyLich { get; set; }

		public string Url_FileUpload_CongVanGioiThieu { get; set; }

		public string Url_FileUpload_GiaySucKhoe { get; set; }

		public string Url_FileUpload_HopDongLaoDong { get; set; }

		public string Url_FileUpload_ThuGioiThieu { get; set; }

		public string Url_FileUpload_ThuGioiThieu2 { get; set; }

		public string Url_FileUpload_BaiBaoKhoaHoc { get; set; }

		public string Url_FileUpload_DeCuongNghienCuu { get; set; }

		public string Url_FileUpload_Zip { get; set; }

		public string TenNganh { get; set; }

		public string TenKhoa { get; set; }

		public int? NganhId { get; set; }

		public int? KhoaId { get; set; }

		public int? IdDotTS { get; set; }

		public string DanToc { get; set; }

		public int? TrangThaiTuyenSinh { get; set; }

		public double? DiemDeCuong { get; set; }

		public double? DiemTong { get; set; }

		public string QuocTich { get; set; }

		public string MaHoSo { get; set; }

		public bool? HPBoSung { get; set; }

		public int? SoCongTrinhKhoaHoc { get; set; }

		public double? NgoaiNgu_Diem { get; set; }

		public string NgoaiNgu_NoiCap { get; set; }

		public DateTime? NgoaiNgu_NgayCap { get; set; }

		public List<nghienCuuSinh1P> ncsl { get; set; }
	}
}