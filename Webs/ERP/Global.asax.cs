﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using App.Erp.Infrastructures;
using App.Erp.Providers;
using Autofac.Integration.Mvc;
using Core.Constant;
using Core.Helper;

namespace App.Erp
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_BeginRequest()
        {
            if (!Request.IsLocal && !Request.IsSecureConnection && SettingProvider.GetValue(Settings.REQUIRE_SSL).ToBool())
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
        }

        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            AreaRegistration.RegisterAllAreas();
            DependencyRegistrar.ConfigureContainer();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(DependencyRegistrar.Container));
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
