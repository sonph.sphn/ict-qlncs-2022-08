﻿using Core.Providers;

namespace App.Erp.Providers
{
    public class BaseSettingProvider : ISettingProvider
    {
        public string GetValue(string key, string defaultValue = "")
        {
            return SettingProvider.GetValue(key, defaultValue);
        }
    }
}