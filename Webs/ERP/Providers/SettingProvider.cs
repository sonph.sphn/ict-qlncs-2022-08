﻿using System.Collections.Generic;
using System.Linq;
using App.Erp.Infrastructures;
using Core.Domain.System;
using Core.Services.System;

namespace App.Erp.Providers
{
    public static class SettingProvider
    {
        private static List<SystemSetting> _settings = DependencyRegistrar.Get<ISettingService>().Gets().ToList();

        public static string GetValue(string key, string defaultValue = "")
        {
            return _settings.FirstOrDefault(i => i.Key == key)?.Value ?? defaultValue;
        }

        public static void ClearCache()
        {
            _settings = DependencyRegistrar.Get<ISettingService>().Gets().ToList();
        }
    }
}
