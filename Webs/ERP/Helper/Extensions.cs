﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml.Serialization;
using App.Erp.Infrastructures;
using App.Erp.Providers;
using Core.Constant;
using Core.Helper;
using OfficeOpenXml;

namespace App.Erp.Helper
{
    public static class Extensions
    {
        public static IEnumerable<SelectListItem> Gets(this List<KeyValuePair<string, int>> data)
        {
            return new SelectList(data, "Value", "Key");
        }
        public static string GetKey(this int value, List<KeyValuePair<string, int>> data, List<KeyValuePair<string, int>> defaut = null)
        {
            if (data.Any(j => j.Value == value)) return data.First(j => j.Value == value).Key;
            return defaut?.First(j => j.Value == value).Key ?? "Undefined";
        }
        public static IEnumerable<SelectListItem> Gets(this List<KeyValuePair<string, string>> data)
        {
            return new SelectList(data, "Value", "Key");
        }
        public static string GetKey(this string value, List<KeyValuePair<string, string>> data)
        {
            return data.Any(j => j.Value == value) ? data.First(j => j.Value == value).Key : "Undefined";
        }
        public static IEnumerable<SelectListItem> Gets(this List<KeyValuePair<string, bool>> data)
        {
            return new SelectList(data, "Value", "Key");
        }
        public static string GetKey(this bool value, List<KeyValuePair<string, bool>> data)
        {
            return data.Any(j => j.Value == value) ? data.First(j => j.Value == value).Key : "Undefined";
        }

        public static string GetExportContent(this string content)
        {
            if (content.IndexOf("{{day}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{day}}", DateTime.Now.Day.ToString());
            }
            if (content.IndexOf("{{month}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{month}}", DateTime.Now.Month.ToString());
            }
            if (content.IndexOf("{{year}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{year}}", DateTime.Now.Year.ToString());
            }
            if (content.IndexOf("{{logo}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{logo}}", $"<img style='width:100%' src='{SettingProvider.GetValue(Settings.BASE_URL)}{SettingProvider.GetValue(Settings.EXPORT_LOGO_URL)}'/>");
            }
            if (content.IndexOf("{{companyname}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companyname}}", SettingProvider.GetValue(Settings.COMPANY_NAME));
            }
            if (content.IndexOf("{{companyaddress}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companyaddress}}", SettingProvider.GetValue(Settings.COMPANY_ADDRESS));
            }
            if (content.IndexOf("{{companyphone}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companyphone}}", SettingProvider.GetValue(Settings.COMPANY_PHONE));
            }
            if (content.IndexOf("{{companyfax}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companyfax}}", SettingProvider.GetValue(Settings.COMPANY_FAX));
            }
            if (content.IndexOf("{{companyemail}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companyemail}}", SettingProvider.GetValue(Settings.COMPANY_EMAIL));
            }
            if (content.IndexOf("{{companywebsite}}", StringComparison.OrdinalIgnoreCase) > 0)
            {
                content = content.Replace("{{companywebsite}}", SettingProvider.GetValue(Settings.COMPANY_WEBSITE));
            }
            return content;
        }

        public static void ExportWord(this BaseController controller, string content, string name)
        {
            content = GetExportContent(content);
            var strBody = new StringBuilder();
            strBody.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='UTF-8'>");
            strBody.Append("<!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:Zoom>100</w:Zoom><w:DoNotOptimizeForBrowser/></w:WordDocument></xml><![endif]-->");
            strBody.Append("<style>" +
                           "<!-- /* Style Definitions */@page Section1 {size:8.27in 11.69in; margin:0cm 0cm 0cm 0cm ; mso-header-margin:0in; mso-footer-margin:0in; mso-paper-source:0;} div.Section1 {page:Section1;}-->" +
                           ".tableData tr td{border: 1px solid black;padding:3px;}" +
                           ".tableData{border-spacing:0px;border-collapse: collapse;}" +
                           "</style></head>");
            strBody.Append("<body style='tab-interval:0in'>");
            strBody.Append(content);
            strBody.Append("</body></html>");
            controller.Response.AppendHeader("Content-Type", "application/msword");
            controller.Response.AppendHeader("Content-disposition", $"attachment; filename={Common.UnicodeToAscii(name.Replace(' ', '-'))}.doc");
            controller.Response.Write(strBody.ToString());
        }

        public static void ExportExcel(this BaseController controller, string name, ExcelPackage excel)
        {
            controller.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            controller.Response.AddHeader("content-disposition", $"attachment;filename={Common.UnicodeToAscii(name).Replace(" ", "-")}.xls");
            controller.Response.BinaryWrite(excel.GetAsByteArray());
        }

        public static void ExportPdf(this BaseController controller, string content, string name)
        {
            var pdfBytes = new NReco.PdfGenerator.HtmlToPdfConverter().GeneratePdf("<html><head><meta charset='UTF-8''/></head><body>" + content + "</body></html>");
            controller.Response.AppendHeader("Content-Type", "application/octet-stream");
            controller.Response.AppendHeader("Content-disposition", $"attachment; filename={Common.UnicodeToAscii(name.Replace(' ', '-'))}.pdf");
            controller.Response.BinaryWrite(pdfBytes);
            controller.Response.Flush();
        }

        public static void ExportHtml(this BaseController controller, string content, string name)
        {
            var html = "<html><head><meta charset='UTF-8''/></head><body>" + content + "</body></html>";
            controller.Response.AppendHeader("Content-Type", "text/html");
            controller.Response.AppendHeader("Content-disposition", $"attachment; filename={Common.UnicodeToAscii(name.Replace(' ', '-'))}.html");
            controller.Response.Write(html);
            controller.Response.Flush();
        }

        public static void ExportXml<T>(this BaseController controller, T objectToSerialise, string name) where T : new()
        {
            var output = new StringWriter(new StringBuilder());
            var xs = new XmlSerializer(objectToSerialise.GetType());
            var ns = new XmlSerializerNamespaces();
            xs.Serialize(output, objectToSerialise, ns);
            controller.Response.AppendHeader("Content-Type", "text/xml");
            controller.Response.AppendHeader("Content-disposition", $"attachment; filename={Common.UnicodeToAscii(name.Replace(' ', '-'))}.xml");
            controller.Response.Write(output);
            controller.Response.Flush();
        }
    }
}