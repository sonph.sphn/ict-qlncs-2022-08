﻿using System.Web.Mvc;
using App.Erp.Infrastructures;

namespace App.Erp.Controllers
{
    public class DashboardController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}