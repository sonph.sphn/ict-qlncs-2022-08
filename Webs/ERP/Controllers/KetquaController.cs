﻿using App.Erp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;

namespace App.Erp.Views.Ketqua
{
    public class KetquaController : Controller
    {
        // GET: Chuyen
        public ActionResult Index(string SBD)
        {
            ViewBag.Title = "TRA CỨU KẾT QUẢ THI NĂNG KHIẾU VÀO ĐẠI HỌC 2020 - TRƯỜNG ĐẠI HỌC SƯ PHẠM HÀ NỘI";
            KetquaNKDataContext context = new KetquaNKDataContext();
            ViewBag.Masager = "Mời bạn nhập Số báo danh dự thi";
            if (string.IsNullOrEmpty(SBD))
            {
                ViewBag.Masager = "Bạn phải nhập Số báo danh dự thi";
                return View();
            }
            else
            {
                
                var row = context.Hnue_KetquaNKs.FirstOrDefault(k=>k.SBD==SBD);
                if (row is null)
                {
                    ViewBag.Masager = "Không tìm thấy kết quả";
                    ViewBag.Result = "";
                    return View();
                }
                else
                {
                    ViewBag.Result = "Kết quả:";
                    ViewBag.Masager = "";
                    ViewBag.Hoten = "Họ và tên: "+ row.Hoten;
                    ViewBag.Ngaysinh = "Ngày sinh:" + row.Ngaysinh;
                    ViewBag.Gioitinh = "Giới tính: "+row.Gioi;
                    ViewBag.SoCMT = "Số CMT: "+row.CMT;
                    ViewBag.NganhDK = "Ngành ĐK: "+row.NganhDK;
                    ViewBag.Dotuong = "Đối tượng: " + row.Dotuong;
                    ViewBag.Khuvuc = "Khu vực: " + row.Khuvuc;
                    ViewBag.MonNK1 = "Môn NK1: " + row.MonNK1;
                    ViewBag.MonNK2 = "Môn NK2: " + row.MonNK2;
                    ViewBag.UutienDT = "Điểm ưu tiên: " + row.UTDTKV;
                    ViewBag.HK = "Học lực 12: " + row.HL12;
                    ViewBag.TBCTN = "TBC TN: " + row.TBCNT;
                    ViewBag.Tongdiem = "Tổng điểm: " + row.Tongdiem;
                    return View(row); 
                }
            }
        }
    }
}