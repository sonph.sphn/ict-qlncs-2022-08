﻿using App.Erp.Providers;
using Core.Domain.Organization;
using Core.Helper;
using Core.Model.Security;
using Core.Services.Organize;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;

namespace App.Erp.Controllers
{
    public class DangkyController : Controller
    {
        private readonly IXtt2020Service _xtt2020Service;
        private readonly ICareerService _careerService;
        public DangkyController(IXtt2020Service xtt2020Service, ICareerService careerService)
        {
            _xtt2020Service = xtt2020Service;
            _careerService = careerService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "ĐĂNG KÝ TUYỂN SINH ĐẠI HỌC 2020 - TRƯỜNG ĐẠI HỌC SƯ PHẠM HÀ NỘI";
            ViewBag.Careers = _careerService.Gets().OrderBy(i => i.CareerName).ToList();
            return View();
        }

        public ActionResult Registra(Xtt2020 model)
        {
            //if (!Request.IsLocal)
            //{
            //    var httpclient = new HttpClient();
            //    var parameters = new Dictionary<string, string> { { "secret", SettingProvider.GetValue("G_RECAPCHA_PRIVATE") }, { "response", model.Capcha } };
            //    var encodedContent = new FormUrlEncodedContent(parameters);
            //    var response = httpclient.PostAsync(SettingProvider.GetValue("G_RECAPCHA_VERIFY"), encodedContent).Result;
            //    var result = response.Content.ReadAsStringAsync().Result.CastJson<TokenResponseModel>();
            //    if (!result.Success)
            //    {
            //        return RedirectToAction("Capcha");
            //    }
            //}
            if (string.IsNullOrEmpty(model.HoTen)|| string.IsNullOrEmpty(model.NgaySinh)|| string.IsNullOrEmpty(model.CMT)|| string.IsNullOrEmpty(model.MaNganh) || string.IsNullOrEmpty(model.HoKhau) || string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.DT))
            {
                return RedirectToAction("UnSuccess");
            }
            if (string.Equals(model.Khuvuc,"00")|| string.Equals(model.Doituong,"00")||string.Equals(model.NganhDK,"00"))
            {
                return RedirectToAction("UnSuccess");
            }
            _xtt2020Service.Insert(model);
            return RedirectToAction("Success");
        }

        public ActionResult Capcha()
        {
            return View();
        }
        public ActionResult Success()
        {
            return View();
        }
        public ActionResult UnSuccess()
        {
            return View();
        }
    }
}