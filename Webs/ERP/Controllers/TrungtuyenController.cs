﻿using App.Erp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;

namespace App.Erp.Views.Ketqua
{
    public class TrungtuyenController : Controller
    {
        // GET: Chuyen
        public ActionResult Index(string CMT)
        {
            ViewBag.Title = "TRA CỨU THÍ SINH TRÚNG TUYỂN ĐẠI HỌC NĂM 2020 XÁC NHẬN NHẬP HỌC VÀO TRƯỜNG ĐẠI HỌC SƯ PHẠM HÀ NỘI";
            KetquaNKDataContext context = new KetquaNKDataContext();
            ViewBag.Masager = "Mời bạn nhập số chứng minh thư hoặc thể căn cước";
            if (string.IsNullOrEmpty(CMT))
            {
                ViewBag.Masager = "Bạn phải nhập số chứng minh thư hoặc thể căn cước";
                return View();
            }
            else
            {
                
                var row = context.Hnue_NhaphocTs.FirstOrDefault(k=>k.CMT==CMT);
                if (row is null)
                {
                    ViewBag.Masager = "Không tìm thấy kết quả";
                    ViewBag.Result = "";
                    return View();
                }
                else
                {
                    ViewBag.Result = "Kết quả:";
                    ViewBag.Masager = "";
                    ViewBag.Hoten = "Họ và tên: "+ row.Hoten;
                    ViewBag.SBD = "Số báo danh: " + row.SBD;
                    ViewBag.CMT = "Số CMT: " + row.CMT;
                    ViewBag.Gioi = "Giới tính: " + row.Gioi;
                    ViewBag.Ngaysinh = "Ngày sinh: " + row.NgaySinh;
                    ViewBag.MaNganhTT = "Mã ngành trúng tuyển: " + row.MaNganh;
                    ViewBag.TenNganhTT = "Tên ngành trúng tuyển: " + row.TenNganh;
                    ViewBag.DienTT  = "Diện trúng tuyển: " + row.DienTT;
                    ViewBag.Welcome = "Hẹn gặp em tại ngày nhập học!";
                    return View(row); 
                }
            }
        }
    }
}