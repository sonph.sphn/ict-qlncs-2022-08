﻿using System.Linq;
using System.Web.Mvc;
using Core.Helper;
using Core.Services.Organize;
using Core.Services.System;
using RestSharp;

namespace App.Erp.Controllers
{
    [Authorize]
    public class SharedController : Controller
    {
        private readonly IProvinceService _provinceService;
        private readonly IUserService _userService;
        public SharedController(IUserService userService, IProvinceService provinceService)
        {
            _provinceService = provinceService;
            _userService = userService;
        }

        public JsonResult GetProvinces(int parentId = 0)
        {
            return Json(_provinceService.GetChildren(parentId).OrderBy(i => i.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoLocation(string term = "", int parentId = 0)
        {
            return Json(_provinceService.GetChildren(parentId, keyword: term).OrderBy(i => i.Name).Select(i => new { id = i.Id, value = i.Name }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteUser(string term = "")
        {
            var data = _userService.Gets(keyword: term).ToList();
            return Json(data.Select(i => new
            {
                id = i.Id,
                value = i.Name
            }).ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MoneyToText(string m)
        {
            return Json(Common.ToNumberString(m).Replace("  ", " ").FirstUpper(), JsonRequestBehavior.AllowGet);
        }

        public string CheckShipmentStatus(string token, string url)
        {
            var client = new RestClient();
            var request = new RestRequest(url);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Token", token);
            return client.Post(request).Content;
        }

        public ActionResult Print()
        {
            return View();
        }
    }
}