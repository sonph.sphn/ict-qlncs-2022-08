﻿using NCS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;
using App.Erp.Models;

namespace App.Erp.Controllers
{
    public class QuanLiNCSController : Controller
    {
        public DataClasses1DataContext db = new DataClasses1DataContext();
        // GET: QuanLiNCS
        public ActionResult Index(int? page)
        {
            /*int pageSize = 5;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<nghienCuuSinh1P> ncss = null;
            BL businessLogic = new BL();
            nghienCuuSinh1P objNCS = new nghienCuuSinh1P();
            List<nghienCuuSinh1P> objNCSList = new List<nghienCuuSinh1P>();
            objNCSList = businessLogic.nghienCuuSinh1PListBind();
            objNCS.ncsl = objNCSList;
            ncss = objNCSList.ToPagedList(pageIndex, pageSize);*/
            var nghienCuuSinh = db.DangKyTuyenSinhs.ToList();
            ViewBag.NCS = nghienCuuSinh;
            return View();
        }
        public ActionResult Them()
        {
            return View();
        }
        public ActionResult Xem()
        {
            return View();
        }
        public ActionResult Xoa()
        {
            return RedirectToAction("Index");
        }
    }
}