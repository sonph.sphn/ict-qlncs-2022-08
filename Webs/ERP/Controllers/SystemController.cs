﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using App.Erp.Context;
using App.Erp.Security;
using Core.Domain.System;
using Core.Helper;
using Core.Model.Common;
using Core.Model.Security;
using Core.Model.User;
using Mvc.JQuery.DataTables.DynamicLinq;
using Core.Services.System;
using App.Erp.Infrastructures;
using App.Erp.Providers;
using Core.Services.Organize;
using Core.Model.Organization;
using System.Collections.Generic;
using Core.Domain.Organization;

namespace App.Erp.Controllers
{
    [Admin]
    public class SystemController : BaseController
    {
        private readonly IRoleService _roleService;
        private readonly IRoleFunctionService _roleFunctionService;
        private readonly IUserRoleService _userRoleService;
        private readonly IUserService _userService;
        private readonly IFunctionService _functionService;
        private readonly IFunctionCategoryService _functionCategoryService;
        private readonly ISettingService _settingService;
        private readonly ISettingGroupService _groupService;
        private readonly IProvinceService _provinceService;
        public SystemController(IUserService userService, IProvinceService provinceService, IFunctionCategoryService functionCategoryService, IFunctionService functionService, IRoleService roleService, IRoleFunctionService roleFunctionService, IUserRoleService userRoleService, ISettingService settingService, ISettingGroupService groupService)
        {
            _userService = userService;
            _functionCategoryService = functionCategoryService;
            _functionService = functionService;
            _roleService = roleService;
            _roleFunctionService = roleFunctionService;
            _userRoleService = userRoleService;
            _settingService = settingService;
            _groupService = groupService;
            _provinceService = provinceService;
        }

        [Admin]
        public ActionResult Province(int parentId = 0, string keyword = "")
        {
            ViewBag.ParentId = parentId;
            ViewBag.Keyword = keyword;
            return View();
        }

        [Admin]
        public object GetProvinces(TableModel model, int parentId = 0, string keyword = "")
        {
            var lst = _provinceService.Gets().Where(i => i.ParentId == parentId);
            if (!string.IsNullOrEmpty(keyword))
            {
                lst = lst.Where(i => i.Name.Contains(keyword));
            }
            lst = lst.OrderBy(model.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        [Admin]
        public ActionResult ProvinceEdit(int id = 0, int parentId = 0)
        {
            var lst = _provinceService.GetTree();
            lst.Insert(0, new KeyValuePair<string, int>("Root", 0));
            var m = new ProvinceModel
            {
                Id = id,
                Provinces = new SelectList(lst, "Value", "Key"),
                ParentId = parentId
            };
            if (id == 0) return View(m);
            var u = _provinceService.Get(id);
            m.Name = u.Name;
            m.ParentId = u.ParentId;
            return View(m);
        }

        [Admin]
        [HttpPost]
        public ActionResult ProvinceEdit(ProvinceModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var u = new Province
            {
                Id = model.Id,
                Name = model.Name,
                ParentId = model.ParentId
            };
            u.Id = _provinceService.InsertOrUpdate(u);
            return IframeScript;
        }

        [Admin]
        public ActionResult ProvinceDelete(int id)
        {
            var u = _provinceService.Get(id);
            _provinceService.Delete(u);
            return RedirectToAction("Province");
        }

        #region Setting Group
        public ActionResult SettingGroup()
        {
            return View();
        }

        public object GetSettingGroups(TableModel model)
        {
            var lst = _groupService.Gets();
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult SettingGroupEdit(int id = 0)
        {
            var m = new SettingGroupModel();
            if (id == 0) return View(m);
            var f = _groupService.Get(id);
            f.CopyTo(m);
            return View(m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SettingGroupEdit(SettingGroupModel m)
        {
            if (!ModelState.IsValid) return View(m);
            var f = _groupService.Get(m.Id) ?? new SystemSettingGroup();
            m.CopyTo(f);
            _groupService.InsertOrUpdate(f);
            return IframeScript;
        }

        public ActionResult SettingGroupDelete(int id)
        {
            _groupService.Delete(id);
            return RedirectToAction("SettingGroup");
        }
        #endregion

        #region Setting
        public ActionResult Setting(int groupId = 0)
        {
            var groups = _groupService.Gets();
            ViewBag.Groups = groups;
            ViewBag.GroupId = groupId > 0 ? groupId : groups.FirstOrDefault()?.Id;
            return View();
        }

        public object GetSettings(TableModel model, int groupId)
        {
            var lst = _settingService.Gets(groupId);
            lst = lst.OrderBy(model.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult SettingEdit(int id = 0)
        {
            ViewBag.Groups = _groupService.Gets();
            var m = new SettingModel();
            if (id == 0) return View(m);
            _settingService.Get(id).CopyTo(m);
            return View(m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SettingEdit(SettingModel model)
        {
            var d = _settingService.Get(model.Id) ?? new SystemSetting();
            model.CopyTo(d);
            _settingService.InsertOrUpdate(d);
            SettingProvider.ClearCache();
            return IframeScript;
        }

        public ActionResult SettingDelete(int id)
        {
            var d = _settingService.Get(id);
            _settingService.Delete(d);
            return RedirectToAction("Setting");
        }

        public ActionResult SettingClearCache()
        {
            WorkContext.ClearCache();
            return RedirectToAction("Setting");
        }

        public ActionResult SettingRecycle()
        {
            HttpRuntime.UnloadAppDomain();
            return RedirectToAction("Setting");
        }
        #endregion

        #region  Role
        public ActionResult Role()
        {
            return View();
        }

        public object GetRole(TableModel model)
        {
            var lst = _roleService.Gets().OrderBy(model.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToList()
                .Select(i => new
                {
                    i.Id,
                    i.Name,
                    Functions = i.RolesFunctions.Where(j => !j.IsDeleted).Aggregate("", (c, j) => c + j.Function.Name + ", ")
                }).AsQueryable().ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult RoleEdit(int id = 0)
        {
            var m = new RoleModel();
            if (id == 0) return View(m);
            var f = _roleService.Get(id);
            m.Name = f.Name;
            m.Id = f.Id;
            return View(m);
        }

        [HttpPost]
        public ActionResult RoleEdit(RoleModel m)
        {
            if (!ModelState.IsValid) return View(m);
            var f = new SystemRole
            {
                Name = m.Name,
                Id = m.Id
            };
            _roleService.InsertOrUpdate(f);
            return IframeScript;
        }

        public ActionResult RoleDelete(int id)
        {
            _roleService.Delete(id);
            return RedirectToAction("Role");
        }

        public ActionResult RoleFunctions(int id)
        {
            var role = _roleService.Get(id);
            var model = new RoleFuncModel
            {
                RoleId = id,
                RoleName = role.Name,
                Selecteds = role.RolesFunctions.Where(i => !i.IsDeleted).Select(i => i.FunctionId).ToArray(),
                List = _functionCategoryService.Gets().OrderBy(i => i.Name).ToList()
                .Select(j => new FuncGroup
                {
                    GroupName = j.Name,
                    List = _functionService.Gets().Where(i => i.CategoryId == j.Id && !i.IsAdmin).OrderBy(i => i.Order)
                        .Select(i => new SelectListItem
                        {
                            Text = i.Name,
                            Value = i.Id.ToString()
                        }).ToList()
                }).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult RoleFunctions(RoleFuncModel model)
        {
            if (!ModelState.IsValid) return (View(model));
            var rfs = _roleFunctionService.Gets().Where(i => i.RoleId == model.RoleId).ToList();
            foreach (var rf in rfs)
            {
                _roleFunctionService.Delete(rf);
            }
            var lst = model.Selecteds.Select(i => new RoleFunction
            {
                RoleId = model.RoleId,
                FunctionId = i
            }).ToList();
            _roleFunctionService.InsertRange(lst);
            return IframeScript;
        }

        public ActionResult RoleUsers(int id)
        {
            ViewBag.Enrolleds = _userRoleService.Gets().Where(i => i.RoleId == id).ToList();
            ViewBag.RoleId = id;
            return View();
        }

        public JsonResult GetRoleUsers(int roleId, string keyword)
        {
            var data = _userService.Gets(isAdmin: 2, keyword: keyword).Where(i => i.UsersRoles.Where(j => !j.IsDeleted).All(j => j.RoleId != roleId));
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }

        public void RoleAddUser(int roleId, int userId)
        {
            if (_userRoleService.Gets().Any(i => i.UserId == userId && i.RoleId == roleId)) return;
            _userRoleService.Insert(new UserRole
            {
                UserId = userId,
                RoleId = roleId
            });
        }

        public void RoleAddUsers(int roleId, string userIds)
        {
            foreach (var userId in userIds.GetInts('-'))
            {
                RoleAddUser(roleId, userId);
            }
        }

        public int RoleRemoveUser(int userRoleId)
        {
            _userRoleService.Delete(_userRoleService.Get(userRoleId));
            return userRoleId;
        }
        #endregion

        #region Function
        public ActionResult Function(int categoryId = 0)
        {
            ViewBag.CategoryId = categoryId;
            ViewBag.Categories = _functionCategoryService.Gets().OrderBy(i => i.Name).Select(i => new FilterModel { Id = i.Id, Name = i.Name }).ToList();
            return View();
        }

        public object GetFunction(TableModel model, int categoryId = 0)
        {
            var lst = _functionService.Gets().Where(i => i.CategoryId == categoryId).OrderBy(i => i.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult FunctionEdit(int id = 0, int categoryId = 0)
        {
            var m = new FunctionModel
            {
                CategoryId = categoryId,
                Categories = new SelectList(_functionCategoryService.Gets().OrderBy(i => i.Name).Select(i => new FilterModel { Id = i.Id, Name = i.Name }).ToList(), "Id", "Name")
            };
            if (id == 0) return View(m);
            var f = _functionService.Get(id);
            f.CopyTo(m);
            return View(m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FunctionEdit(FunctionModel m)
        {
            var f = _functionService.Get(m.Id) ?? new SystemFunction();
            m.CopyTo(f);
            _functionService.InsertOrUpdate(f);
            return IframeScript;
        }

        public ActionResult FunctionSort(int[] ids)
        {
            var index = 1;
            foreach (var p in ids.Select(i => _functionService.Get(i)))
            {
                p.Order = index++;
                _functionService.Update(p);
            }
            return Json(ids);
        }

        public ActionResult FunctionDelete(int id)
        {
            var cat = _functionService.Get(id);
            _functionService.Delete(cat);
            return RedirectToAction("Function", new { categoryId = cat.CategoryId });
        }
        #endregion

        #region Function Category
        public ActionResult FunctionCategory()
        {
            return View();
        }

        public object GetFunctionCategories(TableModel model)
        {
            var lst = _functionCategoryService.Gets().OrderBy(i => i.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult FunctionCategoryEdit(int id = 0)
        {
            var m = new FunctionCategoryModel
            {
                Id = id
            };
            if (id == 0) return View(m);
            var f = _functionCategoryService.Get(id);
            m.Name = f.Name;
            m.Icon = f.Icon;
            m.Id = f.Id;
            m.Order = f.Order;
            return View(m);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FunctionCategoryEdit(FunctionCategoryModel m)
        {
            if (!ModelState.IsValid) return View(m);
            var f = new SystemCategory
            {
                Name = m.Name,
                Id = m.Id,
                Icon = m.Icon,
                Order = m.Order
            };
            _functionCategoryService.InsertOrUpdate(f);
            return IframeScript;
        }

        public ActionResult FunctionCategorySort(int[] ids)
        {
            var index = 1;
            foreach (var p in ids.Select(i => _functionCategoryService.Get(i)))
            {
                p.Order = index++;
                _functionCategoryService.Update(p);
            }
            return Json(ids);
        }

        public ActionResult FunctionCategoryDelete(int id)
        {
            _functionCategoryService.Delete(id);
            return RedirectToAction("FunctionCategory");
        }
        #endregion

        #region User
        public new ActionResult User(int departmentId = 0, string keyword = "")
        {
            ViewBag.Keyword = keyword;
            ViewBag.DepartmentId = departmentId;
            return View();
        }

        public object GetUser(TableModel model, int departmentId = 0, string keyword = "")
        {
            var lst = _userService.Gets(keyword);
            lst = lst.OrderBy(model.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToList()
                .Select(i => new
                {
                    i.Id,
                    i.Email,
                    i.Avatar,
                    i.Name,
                    i.Phone,
                    i.Salary,
                    i.Allowance,
                    i.Limited,
                    i.Locked,
                    Rs = i.IsAdmin ? "Administrator" : i.UsersRoles.Aggregate("<ol style='padding:0'>", (c, j) => c + "<li>" + j.Role.Name + "</li>") + "</ol>"
                }).AsQueryable().ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        public ActionResult UserEdit(int id = 0)
        {
            var m = new UserModel();
            if (id == 0) return View(m);
            var u = _userService.Get(id);
            u.CopyTo(m);
            m.Password = m.ConfirmPassword = "";
            return View(m);
        }

        public JsonResult CheckEmail(int userId, string email)
        {
            var u = _userService.GetByEmail(email);
            return Json(u != null && u.Id != userId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UserEdit(UserModel model)
        {
            var u = _userService.Get(model.Id) ?? new SystemUser();
            model.CopyTo(u);
            u.Id = _userService.InsertOrUpdate(u);
            return IframeScript;
        }

        public void UserDelete(int id)
        {
            var u = _userService.Get(id);
            _userService.Delete(u);
        }

        public ActionResult UserPassword(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [Function("USER_LIST")]
        [HttpPost]
        public JsonResult UserPassword(int id, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword)) return Json(3);
            _userService.ChangePassword(id, newPassword);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}