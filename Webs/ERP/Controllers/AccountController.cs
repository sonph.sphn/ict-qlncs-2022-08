﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Mvc;
using App.Erp.Context;
using App.Erp.Providers;
using Core.Constant;
using Core.Helper;
using Core.Model.Security;
using Core.Services.Security;
using Core.Services.System;
using Microsoft.Ajax.Utilities;
using RestSharp;

namespace App.Erp.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ISignInService _signInService;
        private readonly IUserService _userService;
        public AccountController(IAuthenticationService authenticationService, ISignInService signInService, IUserService userService)
        {
            _authenticationService = authenticationService;
            _signInService = signInService;
            _userService = userService;
        }

        public ActionResult RecoverPassword(string token)
        {
            return View(new RecoverPasswordModel { Token = token });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RecoverPassword(RecoverPasswordModel model)
        {
            _authenticationService.RecoverPassword(model.Token, model.Password);
            return Json(new
            {
                success = true,
                message = "Mật khẩu của bạn đã được đặt lại."
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetPassword()
        {
            var captcha = new Random().Next(1000, 9999).ToString();
            ViewBag.Image = Convert.ToBase64String(Common.GenerateCapcha(captcha, 130, 50, 20));
            Session["CAPCHA"] = captcha;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(LoginModel model)
        {
            if (model.Capcha != Session["CAPCHA"].ToString())
            {
                return Json(new { success = false, message = "Mã bảo mật không đúng, vui lòng thử lại." }, JsonRequestBehavior.AllowGet);
            }
            _authenticationService.ResetPassword(model.Email, SettingProvider.GetValue(Settings.BASE_URL) + Url.Action("RecoverPassword"));
            return Json(new
            {
                success = true,
                message = "Chúng tôi đã gửi liên kết khôi phục mật khẩu tới email của bạn, vui lòng kiểm tra email và làm theo hướng dẫn."
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {
            var captcha = new Random().Next(1000, 9999).ToString();
            ViewBag.Image = Convert.ToBase64String(Common.GenerateCapcha(captcha, 130, 50, 20));
            Session["CAPCHA"] = captcha;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            try
            {
                if (!Request.IsLocal)
                {
                    var httpclient = new HttpClient();
                    var parameters = new Dictionary<string, string> { { "secret", SettingProvider.GetValue("G_RECAPCHA_PRIVATE") }, { "response", model.Capcha } };
                    var encodedContent = new FormUrlEncodedContent(parameters);
                    var response = httpclient.PostAsync(SettingProvider.GetValue("G_RECAPCHA_VERIFY"), encodedContent).Result;
                    var result = response.Content.ReadAsStringAsync().Result.CastJson<TokenResponseModel>();
                    if (!result.Success)
                    {
                        return RedirectToAction("Login");
                    }
                }
                var v = _authenticationService.ValidateUser(model.Email, model.Password);
                if (!v.success)
                {
                    ModelState.AddModelError("InvalidUsernameOrPassword", v.message);
                    return View(model);
                }
                _signInService.SignIn(model.Email);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Message", new { message = ex.Message + "\n" + ex.InnerException?.Message });
            }
            return Redirect("/");
        }

        [Authorize]
        public ActionResult LogOff()
        {
            _signInService.SignOut();
            WorkContext.ClearCache();
            return RedirectToAction("Index", "Dashboard");
        }

        [Authorize]
        public ActionResult Info()
        {
            return View(WorkContext.User);
        }

        [HttpPost]
        [Authorize]
        public JsonResult ChangePassword(string oldPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword)) return Json(3);
            if (WorkContext.User.Password != Common.Md5(oldPassword)) return Json(2);
            _userService.ChangePassword(WorkContext.User.Id, newPassword);
            return Json(1);
        }
    }
}
