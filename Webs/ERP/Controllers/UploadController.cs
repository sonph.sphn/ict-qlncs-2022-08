﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using App.Erp.Context;
using App.Erp.Providers;
using Core.Constant;
using Core.Helper;
using Core.Model.Common;

namespace App.Erp.Controllers
{
    [Authorize]
    public class UploadController : Controller
    {
        public object EditorUpload()
        {
            if (Request.Files.Count < 1) return null;
            var file = Request.Files[0];
            var fileexts = SettingProvider.GetValue(Settings.ALLOWED_EXTENSIONS).GetSplit('|').ToList();
            if (!fileexts.Contains(Path.GetExtension(file.FileName).ToLower()))
            {
                return $"<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({Request.QueryString["CKEditorFuncNum"]}, '', 'File type is not allowed');</script>";
            }
            var maxsize = SettingProvider.GetValue(Settings.MAX_FILE_SIZE).ToInt();
            if (file.ContentLength / (1024 * 1024) > maxsize)
            {
                return $"<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({Request.QueryString["CKEditorFuncNum"]}, '', 'File is too large');</script>";
            }
            var folder = $"/Files/ContentUpload/{WorkContext.User.Id}/";
            var physicPath = Server.MapPath(folder);
            if (!Directory.Exists(physicPath)) Directory.CreateDirectory(physicPath);
            var filename = Common.GetUniqueFilename(file.FileName);
            file.SaveAs(physicPath + filename);
            var fileurl = folder + filename;
            return $"<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({Request.QueryString["CKEditorFuncNum"]}, '{fileurl}', '');</script>";
        }

        public ActionResult JsUpload(string path)
        {
            if (Request.Files.Count < 1) return null;
            var file = Request.Files[0];
            var fileexts = SettingProvider.GetValue(Settings.ALLOWED_EXTENSIONS).GetSplit('|');
            if (!fileexts.Contains(Path.GetExtension(file.FileName).ToLower()))
            {
                return Json(file.FileName.ToResponseResult(false, "File type is not allowed"));
            }
            var maxsize = SettingProvider.GetValue(Settings.MAX_FILE_SIZE).ToInt();
            if (file.ContentLength / (1024 * 1024) > maxsize)
            {
                return Json(file.FileName.ToResponseResult(false, "File is too large"));
            }
            var folder = $"/Files/{path}/{WorkContext.User.Id}/";
            var physicPath = Server.MapPath(folder);
            if (!Directory.Exists(physicPath)) Directory.CreateDirectory(physicPath);
            var filename = Common.GetUniqueFilename(file.FileName);
            file.SaveAs(physicPath + filename);
            return Json((folder + filename).ToResponseResult());
        }
    }
}