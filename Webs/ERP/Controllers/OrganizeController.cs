﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Erp.Infrastructures;
using App.Erp.Security;
using Core.Domain.Organization;
using Core.Helper;
using Core.Model.Common;
using Core.Model.Organization;
using Core.Services.Organize;
using Mvc.JQuery.DataTables.DynamicLinq;
using Resources;

namespace App.Erp.Controllers
{
    public class OrganizeController : BaseController
    {
        private readonly IProvinceService _provinceService;
        public OrganizeController(IProvinceService provinceService)
        {
            _provinceService = provinceService;
        }

        [Admin]
        public ActionResult Province(int parentId = 0, string keyword = "")
        {
            ViewBag.ParentId = parentId;
            ViewBag.Keyword = keyword;
            return View();
        }

        [Admin]
        public object GetProvinces(TableModel model, int parentId = 0, string keyword = "")
        {
            var lst = _provinceService.Gets().Where(i => i.ParentId == parentId);
            if (!string.IsNullOrEmpty(keyword))
            {
                lst = lst.Where(i => i.Name.Contains(keyword));
            }
            lst = lst.OrderBy(model.Order);
            return lst.Skip(model.Start).Take(model.Pagesize).ToTableResult(lst.Count(), model.Draw).ToJson();
        }

        [Admin]
        public ActionResult ProvinceEdit(int id = 0, int parentId = 0)
        {
            var lst = _provinceService.GetTree();
            lst.Insert(0, new KeyValuePair<string, int>(Actions.Root, 0));
            var m = new ProvinceModel
            {
                Id = id,
                Provinces = new SelectList(lst, "Value", "Key"),
                ParentId = parentId
            };
            if (id == 0) return View(m);
            var u = _provinceService.Get(id);
            m.Name = u.Name;
            m.ParentId = u.ParentId;
            return View(m);
        }

        [Admin]
        [HttpPost]
        public ActionResult ProvinceEdit(ProvinceModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var u = new Province
            {
                Id = model.Id,
                Name = model.Name,
                ParentId = model.ParentId
            };
            u.Id = _provinceService.InsertOrUpdate(u);
            return IframeScript;
        }

        [Admin]
        public ActionResult ProvinceDelete(int id)
        {
            var u = _provinceService.Get(id);
            _provinceService.Delete(u);
            return RedirectToAction("Province");
        }
    }
}