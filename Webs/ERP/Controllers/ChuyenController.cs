﻿using App.Erp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;

namespace App.Erp.Views.Chuyen
{
    public class ChuyenController : Controller
    {
        // GET: Chuyen
        public ActionResult Index(string FullName)
        {
            KetquaTSDataContext context = new KetquaTSDataContext();
            IList<Chuyen2020> _model = new List<Chuyen2020>();
            if (string.IsNullOrEmpty(FullName))
            {
                ViewBag.Masager = "Mời nhập số bao danh";
            }
            else
            {
                ViewBag.Masager = "";
                ViewBag.Result = "Kết quả:";
                var query = (from kqs in context.Hnue_KetquaTS where SqlMethods.Like(kqs.FullName, "%"+ FullName.Trim() + "%") select kqs).ToList();
                if (query.Count > 0)
                {
                    foreach (var item in query)
                    {
                        _model.Add(new Chuyen2020
                        {
                            Id = Convert.ToInt32(item.Id),
                            FullName = item.FullName,
                            Birthday = Convert.ToDateTime(item.Birthday),
                            HomeTown = item.Hometown,
                            Province = item.Province,
                            M1 = item.M1,
                            M2 = item.M2,
                            M3 = item.M3,
                            M4 = item.M4,
                            Total= item.Total,
                            Result = Convert.ToInt16(item.Result)
                        });
                    }
                }
                else
                {
                    ViewBag.Masager = "Không tìm thấy bản ghi nào";
                    ViewBag.Result = "";
                }
            }
            return View(_model);
        }
    }
}