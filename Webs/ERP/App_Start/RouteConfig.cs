﻿using System.Web.Mvc;
using System.Web.Routing;

namespace App.Erp
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Default", "{controller}/{action}", new { Controller = "Dashboard", Action = "Index" },new[] { "App.Erp.Controllers" });
        }
    }
}
