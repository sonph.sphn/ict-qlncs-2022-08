﻿$(function () {
    var bodyWidth = "84%";
    var leftWidth = "16%";
    if ($.cookie("left-panel-width") != undefined) {
        leftWidth = $.cookie("left-panel-width");
        bodyWidth = $.cookie("right-panel-width");
    }
    $(".leftpanel").css("width", leftWidth);
    $(".body-content").css("width", bodyWidth);

    var menuClosed = false;
    if ($.cookie("left-panel-hide") != undefined && $.cookie("left-panel-hide") === "true") {
        menuClosed = true;
        $(".leftpanel").css("margin-left", "-100%");
        $(".body-content").css("width", "100%");
    }
    $("#btnMainmenu").click(function () {
        if (!menuClosed) {
            $(".leftpanel").animate({ "margin-left": "-100%" }, 800);
            $(".body-content").animate({ "width": "100%" }, 800);
            $.cookie("left-panel-hide", "true", { path: "/" });
            menuClosed = true;
        } else {
            $(".leftpanel").animate({ "margin-left": "0%" }, 800);
            $(".body-content").animate({ "width": bodyWidth }, 800);
            $.cookie("left-panel-hide", "false", { path: "/" });
            menuClosed = false;
        }
    });
    $(".leftpanel").resizable({
        autoHide: true,
        handles: "e",
        resize: function (e, ui) {
            var parent = ui.element.parent();
            var remainingSpace = parent.width() - ui.element.outerWidth();
            var divTwo = ui.element.next();
            var divTwoWidth = (remainingSpace - (divTwo.outerWidth() - divTwo.width())) / parent.width() * 100 + "%";
            divTwo.width(divTwoWidth);
        },
        stop: function (e, ui) {
            var w = ui.element.width() / ui.element.parent().width() * 100;
            ui.element.css(
                {
                    width: w + "%"
                });
            leftWidth = w + "%";
            bodyWidth = (100 - w) + "%";
            $.cookie("left-panel-width", leftWidth, { path: "/" });
            $.cookie("right-panel-width", bodyWidth, { path: "/" });
        }
    });
    var wh = $(window).height() - $(".header").height();
    $(".navigation, .scroll-body").slimScroll({ height: wh, size: "10px", color: "rgb(0, 151, 255) none repeat scroll 0% 0%" });
    $(".logincontent").height(wh);
    $("#mainnav > li > a").click(function () {
        $("#mainnav > li > ul").hide("fast");
        $(this).toggleClass("expanded");
        $(this).next().toggle("slow");
    });
    $("select.checkoption option").each(function () {
        if ($(this).attr("value") === "-1") $(this).attr("disabled", "disabled");
    });
    $("table.table:not('.no-hover')").on('click', 'tr', function () {
        $("table.table tr.active").removeClass("active");
        $(this).addClass("active");
    });
    $('a[data-toggle="tab"]').on("click", function (e) {
        $.cookie("activeTab", $(e.target).attr("href"));
    });
    if ($.cookie("activeTab")) {
        $(`a[href="${$.cookie("activeTab")}"]:not(.no-remember)`).tab("show");
    }
    if (window.location.pathname.split('?')[0] !== "/") {
        $(`#mainnav a[href='${window.location.pathname.split('?')[0]}']`).each(function () {
            $(this).addClass("active");
            $(this).parent().parent().show();
            $(this).parent().parent().prev().addClass("expanded");
        });
    }
    $(".btn-return-modal").click(function () {
        $("#myModal").modal("show");
        $(this).hide();
    });
    setGlobalMessage("");
    $("#btn-right-close").click(function () {
        $(this).parent().removeClass("active");
        $("#right-body").html('<img class="right-loading" src="/wwwroot/images/loading.svg" />');
    });
});

function showModal(e) {
    return showModalUrl($(e).attr('href'));
}

function showRightPanel(e) {
    return showRightPanelUrl($(e).attr('href'));
}

function showRightPanelUrl(url) {
    $("#right-content").addClass("active");
    setTimeout(function () { $("#right-body").html(`<iframe id='modalForm' style='width:100%;height:88vh;border:none' src='${url}'></iframe>`) }, 1000);
    return false;
}

function showModalUrl(url) {
    showModalLoading();
    $("#myModal").modal({
        backdrop: "static",
        keyboard: false
    }).modal("show");
    $("#myModal #btnClosePopup").click(function () { closeModal(); });
    $("#myModal #btnHidePopup").click(function () {
        $("#myModal").modal("hide");
        $(".btn-return-modal").show();
    });
    $(".modal-body").html(`<iframe id='modalForm' style='width:100%;height:100%;border:none' src='${url}'></iframe>`);
    return false;
}

function showCalendarModal(url) {
    $("#myModal").modal({
        backdrop: "static",
        keyboard: false
    }).modal("show");
    $("#myModal #btnClosePopup").click(function () {
        $("#myModal").modal("hide");
        $("#calendar").fullCalendar('refetchEvents');
    });
    $(".modal-body").html(`<iframe id='modalForm' style='width:100%;height:100%;border:none' src='${url}'></iframe>`);
    return false;
}

var modalCompletedCallback = undefined;
function setModalCompletedCallback(callback) {
    modalCompletedCallback = callback;
}

$(document).on("click", ".tbl-delete-btn", function () {
    if (confirm("Bạn có chắc muốn xoá không?")) {
        $.post($(this).attr("href")).done(function () {
            if (modalCompletedCallback != undefined) {
                modalCompletedCallback();
            }
        });
    }
    return false;
});

function closeModal() {
    $("#myModal").modal("hide");
    if (modalCompletedCallback == undefined || modalCompletedCallback == null) {
        $("#page-loading").show();
        window.location.href = window.location.href;
    } else {
        modalCompletedCallback();
    }
}

function closeModal(reload) {
    $("#myModal").modal("hide");
    if (reload && modalCompletedCallback == undefined) {
        $("#page-loading").show();
        window.location.href = window.location.href;
    } else {
        modalCompletedCallback();
    }
}

function setModalSize(width, height) {
    $(".modal-dialog").css("width", width);
    $(".modal-dialog").css("height", height);
}

function closeModalLoading() {
    $("#poploading").hide("fast");
}

function showModalLoading() {
    $("#poploading").show();
}

function setGlobalMessage(msg) {
    $("#global-message").html(msg);
}