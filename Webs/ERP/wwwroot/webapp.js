﻿$(function () {
    $("input.number-digit").digits();
    $(document).on("focus", "input.number-digit", function () {
        $(this).val($(this).val().replace(/,/g, ""));
    });
    $(document).on("focusout", "input.number-digit", function () {
        if ($(this).val().trim() === "") {
            $(this).val("0");
        } else {
            $(this).digits();
        }
    });
    var ctrlDown = false;
    $(document).on("keydown", "input[code=number], input.number, input.number-digit", function (e) {
        //CTRL + 
        if (e.keyCode === 17 || e.keyCode === 91) {
            ctrlDown = true;
            return false;
        }
        //CTRL + V/C
        if (ctrlDown && (e.keyCode === 86 | e.keyCode === 67)) {
            return true;
        }
        if (e.keyCode === 8 || e.keyCode === 13 || e.keyCode === 9 || e.keyCode === 116 || e.keyCode === 190 || e.keyCode === 110 || e.keyCode === 109 || e.keyCode === 173)
            return true;
        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))
            return true;
        if (isNaN(this.value + "" + String.fromCharCode(e.keyCode)))
            return false;
        return true;
    }).on("keyup", "input[code=number], input.number, input.number-digit", function (e) {
        if (e.keyCode === 17 || e.keyCode === 91) ctrlDown = false;
    });
    $(".datetimepicker").datetimepicker({
        format: 'd-m-Y H:i',
        formatDate: 'd-m-Y'
    });
    $(".datepicker").datetimepicker({
        timepicker: false,
        format: 'd-m-Y',
        formatDate: 'd-m-Y'
    });
    $(".timepicker").datetimepicker({
        datepicker: false,
        format: 'H:i',
        step: 5
    });
    $("select").each(function () {
        $(this).change(function () {
            $("select[parent=" + $(this).attr("id") + "]").val(0);
            $("select[parent=" + $(this).attr("id") + "]").change();
        });
        $(this).click(function () {
            if ($(this).attr("parent") !== undefined) {
                var parentValue = $("#" + $(this).attr("parent")).val();
                $(this).children().each(function () {
                    if ($(this).attr("value") != 0 && $(this).attr("parentId") == parentValue) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        });
    });
    $("select").each(function () {
        if ($(this).attr("value") !== undefined && $(this).attr("value") !== "" && $(this).attr("value") !== "0") {
            $(this).val($(this).attr("value"));
        }
        if ($(this).attr("trigger-on-start") != undefined) {
            $(this).trigger($(this).attr("trigger-on-start"));
        }
    });
});
function showPageLoading() {
    $("#page-loading").show();
}
function hidePageLoading() {
    $("#page-loading").hide();
}
$.fn.digits = function (fix) {
    this.each(function () {
        if (fix < 0) fix = 0;
        if ($(this).html() !== "") {
            $(this).html(parseFloat($(this).html().replace(/,/g, "")).toFixed(fix).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
        if ($(this).val() !== "") {
            $(this).val(parseFloat($(this).val().replace(/,/g, "")).toFixed(fix).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
    });
}
function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}
function acronym(str) {
    var arr = str.split(" ");
    var r = "";
    $.each(arr, function (index, i) {
        r += i[0].toUpperCase();
    });
    return r;
}
function animateCss(element, animationName, callback) {
    var node = document.querySelector(element);
    node.classList.add('animated', animationName);
    function handleAnimationEnd() {
        node.classList.remove('animated', animationName);
        node.removeEventListener('animationend', handleAnimationEnd);
        if (typeof callback === 'function') callback();
    }
    node.addEventListener('animationend', handleAnimationEnd);
}

function getRetailStatus(n) {
    var html = "";
    if (n.Cancelled) {
        if (n.Sent) {
            if (n.Returned) {
                html = `<span class='btn-status' style='background-color:gray;font-size:11px'>Huỷ: Đã hoàn</span>`;
            } else {
                html = `<span class='btn-status' style='background-color:#ff8f00;font-size:11px'>Huỷ: Chờ hoàn</span>`;
            }
        }
        else {
            html = `<span class='btn-status' style='background-color:gray;font-size:11px'>Huỷ: Chưa giao</span>`;
        }
    }
    else html = `<span class='btn-status' style='background-color:${n.Status.Color};font-size:11px'>${n.Status.Name}</span>`;
    return html;
}