﻿Vue.mixin({
    methods: {
        setLoading: function () {
            showPageLoading();
        },
        setStop: function () {
            hidePageLoading();
        }
    },
    filters: {
        formatDate: function (date) {
            if (date == null) return "";
            return moment(date).format('DD/MM/YYYY');
        },
        formatDateTime: function (date) {
            if (date == null) return "";
            return moment(date).format('DD/MM/YYYY HH:mm');
        },
        formatPrice : function (price) {
            if (price == null) return "";
            return price.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
    }
});