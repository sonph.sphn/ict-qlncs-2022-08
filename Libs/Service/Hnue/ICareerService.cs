﻿using Core.Domain.Organization;
using Core.Services.Base;

namespace Core.Services.Organize
{
    public interface ICareerService : IBaseService<Career>
    {
    }
}