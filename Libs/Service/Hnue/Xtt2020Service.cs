﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Core.Domain.Organization;
using Core.Services.Base;

namespace Core.Services.Organize
{
    public class Xtt2020Service : BaseService<Xtt2020>, IXtt2020Service
    {
        public Xtt2020Service(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }
    }
}