﻿using System.Data.Entity;
using Core.Domain.Organization;
using Core.Services.Base;

namespace Core.Services.Organize
{
    public class CareerService : BaseService<Career>, ICareerService
    {
        public CareerService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }
    }
}