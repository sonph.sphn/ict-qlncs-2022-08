﻿using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public interface IUserRoleService : IBaseService<UserRole>
    {
        IQueryable<UserRole> GetByUser(int userId);
    }
}