﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class FunctionCategoryService : BaseService<SystemCategory>, IFunctionCategoryService
    {
        public FunctionCategoryService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public override IQueryable<SystemCategory> Gets()
        {
            return base.Gets().OrderBy(i => i.Name);
        }

    }
}