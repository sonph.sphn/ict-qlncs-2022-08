﻿using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public interface ISettingService : IBaseService<SystemSetting>
    {
        IQueryable<SystemSetting> Gets(int groupId = 0);
        string GetValue(string key);
    }
}