﻿using System.Linq;
using Core.Domain.System;
using Core.Model.Common;
using Core.Services.Base;

namespace Core.Services.System
{
    public interface IUserService : IBaseService<SystemUser>
    {
        int[] GetFunctionIds(int userId);
        SystemUser GetByEmail(string email);
        SystemUser GetByPhone(string phone);
        void ChangePassword(int userId, string newPassword);
        IQueryable<SystemUser> Gets(string keyword = "", int isAdmin = 0);
        ResultData Register(string name, string phone, string email, string password, string avatar, bool locked = true);
        string GenerateUserToken(string email);
    }
}