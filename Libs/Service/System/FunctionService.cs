﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class FunctionService : BaseService<SystemFunction>, IFunctionService
    {
        private readonly IRoleFunctionService _roleFunction;
        public FunctionService(DbContext context, IWorkContextService contextService, IRoleFunctionService roleFunction) : base(context, contextService)
        {
            _roleFunction = roleFunction;
        }

        public override void Delete(SystemFunction entity)
        {
            var rfs = _roleFunction.GetByFunction(entity.Id).ToList();
            foreach (var rf in rfs)
            {
                _roleFunction.Delete(rf);
            }
            base.Delete(entity);
        }
    }
}