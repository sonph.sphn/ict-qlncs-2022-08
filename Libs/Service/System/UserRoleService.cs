﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class UserRoleService : BaseService<UserRole>, IUserRoleService
    {
        public UserRoleService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public IQueryable<UserRole> GetByUser(int userId)
        {
            return Gets().Where(i => i.UserId == userId);
        }

        public override void Delete(UserRole entity)
        {
            HardDelete(entity);
        }
    }
}