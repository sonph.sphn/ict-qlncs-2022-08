﻿using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public interface IRoleFunctionService : IBaseService<RoleFunction>
    {
        IQueryable<RoleFunction> GetByRoles(int[] roleId);
        IQueryable<RoleFunction> GetByFunction(int functionId);
    }
}