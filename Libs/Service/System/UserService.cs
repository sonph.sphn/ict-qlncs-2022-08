﻿using System;
using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Helper;
using Core.Model.Common;
using Core.Services.Base;

namespace Core.Services.System
{
    public class UserService : BaseService<SystemUser>, IUserService
    {
        private readonly IUserRoleService _userRoleService;
        private readonly IRoleFunctionService _roleFunctionService;
        public UserService(DbContext context, IWorkContextService contextService, IUserRoleService userRoleService, IRoleFunctionService roleFunctionService) : base(context, contextService)
        {
            _userRoleService = userRoleService;
            _roleFunctionService = roleFunctionService;
        }

        public SystemUser GetByEmail(string email)
        {
            return base.Gets().FirstOrDefault(i => i.Email.ToLower() == email.ToLower());
        }

        public SystemUser GetByPhone(string phone)
        {
            return base.Gets().FirstOrDefault(i => i.Phone == phone);
        }

        public void ChangePassword(int userId, string newPassword)
        {
            var u = Get(userId);
            u.Password = Common.Md5(newPassword);
            base.Update(u);
        }

        public IQueryable<SystemUser> Gets(string keyword = "", int isAdmin = 0)
        {
            var lst = base.Gets();
            if (!string.IsNullOrEmpty(keyword))
            {
                lst = lst.Where(i => i.Name.Contains(keyword) || i.Email == keyword);
            }
            
            if (isAdmin > 0)
            {
                lst = lst.Where(i => i.IsAdmin == (isAdmin == 1));
            }
            return lst;
        }

        public ResultData Register(string name, string phone, string email, string password, string avatar, bool locked = true)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone))
            {
                return new ResultData(false, "Vui lòng cung cấp email hoặc SĐT!");
            }
            if (!string.IsNullOrEmpty(email) && GetByEmail(email) != null)
            {
                return new ResultData(false, "Email đã được sử dụng!");
            }
            if (!string.IsNullOrEmpty(phone) && GetByPhone(phone) != null)
            {
                return new ResultData(false, "SĐT đã được sử dụng!");
            }
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
            {
                return new ResultData(false, "Vui lòng cung cấp đầy đủ thông tin đăng ký!");
            }
            var u = new SystemUser
            {
                Name = name,
                Phone = phone,
                Email = email,
                Password = Common.Md5(password),
                Avatar = avatar,
                Created = DateTime.Now,
                LastAccess = DateTime.Now,
                Locked = locked
            };
            base.Insert(u);
            return new ResultData(true, Get(u.Id));
        }

        public string GenerateUserToken(string email)
        {
            var u = GetByEmail(email);
            if (u == null) return "";
            u.Token = Guid.NewGuid().ToString();
            u.TokenOn = DateTime.Now;
            base.Update(u);
            return u.Token;
        }

        public override void Update(SystemUser entity)
        {
            var u = GetByEmail(entity.Email);
            if (u != null && u.Id != entity.Id) return;
            var o = base.GetUnProxy(entity.Id);
            entity.Password = o.Password;
            base.Update(entity);
        }

        public override int Insert(SystemUser entity)
        {
            var u = GetByEmail(entity.Email);
            if (u != null && u.Id != entity.Id) return 0;
            entity.Password = Common.Md5(entity.Password);
            return base.Insert(entity);
        }

        public override void Delete(SystemUser entity)
        {
            var urs = _userRoleService.GetByUser(entity.Id).ToList();
            foreach (var ur in urs)
            {
                _userRoleService.Delete(ur);
            }
            base.Delete(entity);
        }

        public int[] GetFunctionIds(int userId)
        {
            var roles = _userRoleService.GetByUser(userId).Select(i => i.RoleId).ToArray();
            return _roleFunctionService.GetByRoles(roles).Select(i => i.FunctionId).ToArray();
        }
    }
}