﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class SettingService : BaseService<SystemSetting>, ISettingService
    {
        public SettingService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public IQueryable<SystemSetting> Gets(int groupId = 0)
        {
            var lst = base.Gets();
            if (groupId > 0)
            {
                lst = lst.Where(i => i.GroupId == groupId);
            }
            return lst;
        }

        public string GetValue(string key)
        {
            return Gets().FirstOrDefault(i => i.Key == key)?.Value ?? "";
        }
    }
}
