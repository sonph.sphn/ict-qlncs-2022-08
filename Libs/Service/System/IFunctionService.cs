﻿using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public interface IFunctionService : IBaseService<SystemFunction>
    {
    }
}