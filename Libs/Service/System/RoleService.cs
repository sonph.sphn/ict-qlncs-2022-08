﻿using System.Data.Entity;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class RoleService : BaseService<SystemRole>, IRoleService
    {
        private readonly IRoleFunctionService _roleFunctionService;
        public RoleService(DbContext context, IWorkContextService contextService, IRoleFunctionService roleFunctionService) : base(context, contextService)
        {
            _roleFunctionService = roleFunctionService;
        }

        public override void Delete(SystemRole entity)
        {
            foreach (var rf in entity.RolesFunctions)
            {
                _roleFunctionService.Delete(rf);
            }
            base.Delete(entity);
        }
    }
}