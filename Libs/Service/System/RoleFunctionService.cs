﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class RoleFunctionService : BaseService<RoleFunction>, IRoleFunctionService
    {
        public RoleFunctionService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public IQueryable<RoleFunction> GetByRoles(int[] roleId)
        {
            return Gets().Where(i => roleId.Contains(i.RoleId));
        }

        public IQueryable<RoleFunction> GetByFunction(int functionId)
        {
            return Gets().Where(i => i.FunctionId == functionId);
        }
    }
}