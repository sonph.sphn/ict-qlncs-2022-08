﻿using System.Data.Entity;
using System.Linq;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.System
{
    public class SettingGroupService : BaseService<SystemSettingGroup>, ISettingGroupService
    {
        public SettingGroupService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public override IQueryable<SystemSettingGroup> Gets()
        {
            return base.Gets().OrderBy(i => i.Name);
        }
    }
}