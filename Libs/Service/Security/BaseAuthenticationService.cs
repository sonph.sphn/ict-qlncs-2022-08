﻿using System.Linq;
using Core.Helper;
using Core.Model.Common;
using Core.Services.System;

namespace Core.Services.Security
{
    public class BaseAuthenticationService : IAuthenticationService
    {
        private readonly IUserService _userService;

        public BaseAuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        public ResultData ValidateUser(string email, string password, string phone = "")
        {
            var u = _userService.GetByEmail(email);
            if (u == null && !string.IsNullOrEmpty(phone))
            {
                u = _userService.GetByPhone(phone);
            }
            if (u == null)
            {
                return new ResultData(false, "Sai tên đăng nhập hoặc mật khẩu!");
            }
            if (u.Locked)
            {
                return new ResultData(false, "Tài khoản chưa được kích hoạt!");
            }
            if (u.Password != Common.Md5(password))
            {
                return new ResultData(false, "Sai tên đăng nhập hoặc mật khẩu!");
            }
            return new ResultData(true, u);
        }

        public void ResetPassword(string email, string urlRecover)
        {
        }

        public void RecoverPassword(string token, string password)
        {
            var u = _userService.Gets().First(i => i.Token == token);
            _userService.ChangePassword(u.Id, password);
        }
    }
}