﻿using Core.Domain.Security;
using Core.Domain.System;
using Core.Services.Base;

namespace Core.Services.Security
{
    public interface ISessionService : IBaseService<SystemSession>
    {
        /// <summary>
        /// SourceId: 1 -> From WEB; 2 -> From API
        /// </summary>
        /// <param name="email"></param>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        string Create(string email, int sourceId);
        bool Validate(string token);
        int GetUserId(string token);
        SystemUser GetUser(string token);
        void Remove(string token);
    }
}