﻿using System.Web;
using System.Web.Security;

namespace Core.Services.Security
{
    public class SignInService : ISignInService
    {
        private readonly ISessionService _sessionService;
        public SignInService(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        public void SignIn(string username)
        {
            var token = _sessionService.Create(username, 1);
            FormsAuthentication.SetAuthCookie(token, true);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.RemoveAll();
            _sessionService.Remove(HttpContext.Current.User.Identity.Name);
        }
    }
}