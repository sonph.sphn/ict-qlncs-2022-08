﻿using System;
using System.Data.Entity;
using System.Linq;
using Core.Domain.Security;
using Core.Domain.System;
using Core.Helper;
using Core.Services.Base;
using Core.Services.System;

namespace Core.Services.Security
{
    public class SessionService : BaseService<SystemSession>, ISessionService
    {
        private readonly IUserService _userService;
        public SessionService(DbContext context, IWorkContextService contextService, IUserService userService) : base(context, contextService)
        {
            _userService = userService;
        }

        public string Create(string email, int sourceId)
        {
            var token = Common.RandomString(128) + Guid.NewGuid();
            Insert(new SystemSession
            {
                CreateOn = DateTime.Now,
                LassAccess = DateTime.Now,
                Token = token,
                UserId = _userService.GetByEmail(email).Id,
                SourceId = sourceId
            });
            return token;
        }

        public bool Validate(string token)
        {
            var s = Gets().FirstOrDefault(i => i.Token == token);
            if (s == null) return false;
            s.LassAccess = DateTime.Now;
            Update(s);
            return true;
        }

        public int GetUserId(string token)
        {
            return GetUser(token)?.Id ?? 0;
        }

        public SystemUser GetUser(string token)
        {
            var session = base.Gets().FirstOrDefault(i => i.Token == token);
            if (session == null) return null;
//            session.LassAccess = DateTime.Now;
//            BaseUpdate(session);
            return _userService.Get(session.UserId);
        }

        public void Remove(string token)
        {
            var s = Gets().FirstOrDefault(i => i.Token == token);
            base.Delete(s);
        }

    }
}