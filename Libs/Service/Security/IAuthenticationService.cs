﻿using Core.Model.Common;

namespace Core.Services.Security
{
    public interface IAuthenticationService
    {
        ResultData ValidateUser(string email, string password, string phone = "");
        void ResetPassword(string email, string urlRecover);
        void RecoverPassword(string token, string password);
    }
}