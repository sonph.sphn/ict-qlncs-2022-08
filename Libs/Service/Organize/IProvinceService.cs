﻿using System.Collections.Generic;
using System.Linq;
using Core.Domain.Organization;
using Core.Services.Base;

namespace Core.Services.Organize
{
    public interface IProvinceService : IBaseService<Province>
    {
        List<KeyValuePair<string, int>> GetTree(int parentId = 0, string sap = "");
        IQueryable<Province> GetChildren(int parentId, string keyword = "");
        IQueryable<Province> GetDistricts();
        IQueryable<Province> GetWards();
    }
}