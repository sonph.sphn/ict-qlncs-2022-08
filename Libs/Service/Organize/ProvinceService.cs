﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Core.Domain.Organization;
using Core.Services.Base;

namespace Core.Services.Organize
{
    public class ProvinceService : BaseService<Province>, IProvinceService
    {
        public ProvinceService(DbContext context, IWorkContextService contextService) : base(context, contextService)
        {
        }

        public List<KeyValuePair<string, int>> GetTree(int parentId = 0, string sap = "")
        {
            var r = new List<KeyValuePair<string, int>>();
            foreach (var i in Gets().OrderBy(i => i.Name).Where(i => i.ParentId == parentId))
            {
                r.Add(new KeyValuePair<string, int>(sap + i.Name, i.Id));
                r.AddRange(GetTree(i.Id, sap + "..."));
            }
            return r;
        }

        public IQueryable<Province> GetChildren(int parentId, string keyword = "")
        {
            return Gets().Where(i => i.ParentId == parentId && i.Name.Contains(keyword)).OrderBy(i => i.Name);
        }

        public IQueryable<Province> GetDistricts()
        {
            var provinceIds = GetChildren(0).Select(i => i.Id).ToArray();
            return Gets().Where(i => provinceIds.Contains(i.ParentId)).OrderBy(i => i.ParentId).ThenBy(i => i.Name);
        }
        public IQueryable<Province> GetWards()
        {
            var provinceIds = GetDistricts().Select(i => i.Id).ToArray();
            return Gets().Where(i => provinceIds.Contains(i.ParentId)).OrderBy(i => i.ParentId).ThenBy(i => i.Name);
        }
    }
}