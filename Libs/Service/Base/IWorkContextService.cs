﻿using System.Collections.Generic;
using Core.Domain.System;

namespace Core.Services.Base
{
    public interface IWorkContextService
    {
        int GetCurrentUserId();
        SystemUser GetCurrentUser();
        List<SystemFunction> GetCurrentUserFunction();
    }
}