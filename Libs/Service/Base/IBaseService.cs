﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Core.Base;

namespace Core.Services.Base
{
    public interface IBaseService<T> where T : BaseEntity
    {
        T Get(object id);
        T GetUnProxy(object id);
        int InsertOrUpdate(T entity);
        int Insert(T entity);
        void InsertRange(IEnumerable<T> entities);
        void Update(T entity);
        void BaseUpdate(T entity);
        void Delete(object id);
        void Delete(T entity);
        IQueryable<T> Gets();
        IQueryable<T> GetsNoCache();
        IQueryable<TU> GetsBy<TU>(Expression<Func<T, bool>> exp, Expression<Func<T, TU>> columns);
    }
}