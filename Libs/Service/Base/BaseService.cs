﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Core.Base;
using Core.Data.Base;

namespace Core.Services.Base
{
    public abstract class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly IBaseRepository<T> _repository;
        private readonly IWorkContextService _contextService;
        protected BaseService(DbContext context, IWorkContextService contextService)
        {
            _contextService = contextService;
            _repository = new BaseRepository<T>(context);
        }
        public virtual T Get(object id)
        {
            return _repository.Get(id);
        }
        public virtual T GetUnProxy(object id)
        {
            return _repository.TableNoTracking.FirstOrDefault(i => i.Id == (int)id);
        }
        public virtual int InsertOrUpdate(T entity)
        {
            if (entity.Id < 1)
            {
                return Insert(entity);
            }
            Update(entity);
            return entity.Id;
        }
        public virtual int Insert(T entity)
        {
            entity.CreatorId = entity.UpdaterId = _contextService.GetCurrentUserId();
            entity.Created = entity.Updated = DateTime.Now;
            _repository.Insert(entity);
            return entity.Id;
        }
        public virtual void InsertRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }
        public virtual void Update(T entity)
        {
            var old = _repository.TableNoTracking.Where(i => i.Id == entity.Id).First();
            entity.CreatorId = old.CreatorId;
            entity.Created = old.Created;
            entity.Updated = DateTime.Now;
            entity.UpdaterId = _contextService.GetCurrentUserId();
            _repository.Update(entity);
        }
        public virtual void BaseUpdate(T entity)
        {
            _repository.Update(entity);
        }
        public virtual void Delete(object id)
        {
            Delete(Get(id));
        }
        public virtual void Delete(T entity)
        {
            if (entity == null) return;
            entity.IsDeleted = true;
            entity.Updated = DateTime.Now;
            entity.UpdaterId = _contextService.GetCurrentUserId();
            _repository.Update(entity);
        }
        public void HardDelete(T entity)
        {
            _repository.Delete(entity);
        }
        public virtual IQueryable<T> Gets()
        {
            var lst = _repository.Table.Where(i => !i.IsDeleted);
            return lst;
        }
        public IQueryable<T> GetsNoCache()
        {
            var lst = _repository.Table.AsNoTracking().Where(i => !i.IsDeleted);
            return lst;
        }
        public IQueryable<TU> GetsBy<TU>(Expression<Func<T, bool>> exp, Expression<Func<T, TU>> columns)
        {
            return Gets().Where(exp).Select(columns);
        }
    }
}