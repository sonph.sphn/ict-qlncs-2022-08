﻿using System.Data.Entity;
using Core.Domain.Organization;
using Core.Domain.Security;
using Core.Domain.System;

namespace Core.Data.Mapping
{
    public class ErpDataContext : DbContext
    {
        public ErpDataContext() : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled  = false;
        }
        public DbSet<Career> Career { get; set; }
        public DbSet<Xtt2020> Xtt2020 { get; set; }
        public DbSet<SystemSession> AuthToken { get; set; }
        public DbSet<SystemSettingGroup> SystemSettingGroup { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<SystemFunction> Functions { get; set; }
        public DbSet<SystemRole> Roles { get; set; }
        public DbSet<UserRole> UsersRoles { get; set; }
        public DbSet<RoleFunction> RolesFunctions { get; set; }
        public DbSet<SystemSetting> Settings { get; set; }
        public DbSet<SystemUser> Users { get; set; }
        public DbSet<SystemCategory> FunctionCategories { get; set; }
    }
}