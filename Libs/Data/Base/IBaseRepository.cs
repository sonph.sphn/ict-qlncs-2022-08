﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Core.Base;

namespace Core.Data.Base
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        T Get(dynamic id);
        void Insert(T entity);
        void Insert(IEnumerable<T> entities);
        void Update(T entity);
        void Delete(T entity);
        void Delete(dynamic id);
        void Delete(IEnumerable<T> entities);
        IQueryable<T> Table { get; }
        IQueryable<T> TableNoTracking { get; }
        void ExecuteCommand(string cmd, params object[] prms);
        DbRawSqlQuery<TQ> ExecuteQuery<TQ>(string sql, params object[] prms);
    }
}