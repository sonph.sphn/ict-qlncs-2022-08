﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using Core.Base;

namespace Core.Data.Base
{
    public sealed class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly DbContext _context;
        private readonly IDbSet<T> _entities;
        public BaseRepository(DbContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }
        public T Get(dynamic id)
        {
            return _entities.Find(id);
        }
        public void Insert(T entity)
        {
            _entities.Add(entity);
            _context.SaveChanges();
        }
        public void Insert(IEnumerable<T> entities)
        {
            foreach (var entity in entities) _entities.Add(entity);
            _context.SaveChanges();
        }
        public void Update(T entity)
        {
            _entities.AddOrUpdate(entity);
            _context.SaveChanges();
        }
        public void Delete(T entity)
        {
            _entities.Remove(entity);
            _context.SaveChanges();
        }
        public void Delete(dynamic id)
        {
            Delete(Get(id));
        }
        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities) _entities.Remove(entity);
            _context.SaveChanges();
        }
        public IQueryable<T> Table => _entities;
        public IQueryable<T> TableNoTracking => _entities.AsNoTracking();
        public void ExecuteCommand(string cmd, params object[] prms)
        {
            _context.Database.ExecuteSqlCommand(cmd, prms);
        }
        public DbRawSqlQuery<TQ> ExecuteQuery<TQ>(string sql, params object[] prms)
        {
            return _context.Database.SqlQuery<TQ>(sql, prms);
        }
    }
}