﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Core.Helper
{
    public static class Extentions
    {
        public static string ToDateString(this DateTime s)
        {
            return $"Ngày {s.Day} tháng {s.Month} năm {s.Year}";
        }

        public static DateTime ToAppDate(this string s)
        {
            return s.ToDateTime("dd-MM-yyyy");
        }

        public static DateTime ToAppDateTime(this string s)
        {
            return s.ToDateTime("dd-MM-yyyy HH:mm");
        }

        public static string ToAppDate(this DateTime d)
        {
            return d.ToString("dd-MM-yyyy");
        }

        public static string ToAppDate(this DateTime? d)
        {
            return d == null ? "" : d.Value.ToString("dd-MM-yyyy");
        }

        public static string ToAppDateTime(this DateTime d)
        {
            return d.ToString("dd-MM-yyyy HH:mm");
        }

        public static string ToAppTimeDate(this DateTime d, string daySap = "-")
        {
            return d.ToString($"HH:mm dd{daySap}MM{daySap}yyyy");
        }

        public static string AddChar(this int number, char c)
        {
            var r = "";
            for (var i = 0; i < number; i++)
            {
                r += c;
            }
            return r;
        }
        public static bool IsNumberic(this char c)
        {
            return Common.IsNumeric(c.ToString(), NumberStyles.Integer);
        }
        public static void CopyTo(this object obj, object to, params string[] ignores)
        {
            if (obj == null) return;
            var fromProperties = obj.GetType().GetProperties().Where(i => !ignores.Contains(i.Name) && i.CanRead);
            var toProperties = to.GetType().GetProperties().Where(i => !ignores.Contains(i.Name) && i.CanWrite).ToList();
            foreach (var fromProperty in fromProperties)
            {
                foreach (var toProperty in toProperties.Where(toProperty => fromProperty.Name == toProperty.Name && fromProperty.PropertyType == toProperty.PropertyType))
                {
                    toProperty.SetValue(to, fromProperty.GetValue(obj));
                    break;
                }
            }
        }
        public static string Format(this int number, string c = ".", int each = 3)
        {
            return Common.GetSplitFormat(number.ToString(), each, c);
        }
        public static string Base64Encode(this Image image)
        {
            using (var m = new MemoryStream())
            {
                image.Save(m, ImageFormat.Jpeg);
                var imageBytes = m.ToArray();
                var base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static T Clone<T>(this T src) where T : new()
        {
            if (src == null) return default(T);
            var tgt = new T();
            foreach (var pS in src.GetType().GetProperties())
            {
                foreach (var pT in tgt.GetType().GetProperties().Where(pT => pT.Name == pS.Name))
                {
                    try
                    {
                        (pT.GetSetMethod()).Invoke(tgt, new[] { pS.GetGetMethod().Invoke(src, null) });
                    }
                    catch
                    {
                        //ignore
                    }
                }
            }
            return tgt;
        }
        public static string StripHtml(this string inputString)
        {
            return Regex.Replace(inputString, "<.*?>", string.Empty);
        }
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static T CastJson<T>(this object obj)
        {
            return JsonConvert.DeserializeObject<T>(obj?.ToString() ?? "");
        }
        public static Stream ToStream(this Image image, ImageFormat formaw)
        {
            var stream = new MemoryStream();
            image.Save(stream, formaw);
            stream.Position = 0;
            return stream;
        }
        public static string ToIconCheck(this bool value, string icon = "glyphicon glyphicon-ok-sign")
        {
            return value ? $"<i class='{icon}'></i>" : "";
        }
        public static string RemoveNonAlphanumberic(this string source)
        {
            return global::Core.Helper.Common.RemoveNonAlphaNumberic(source);
        }
        public static bool IsBirthday(this DateTime birthday)
        {
            return birthday.Day == DateTime.Now.Day & birthday.Month == DateTime.Now.Month;
        }
        public static int ToInt(this object source)
        {
            try
            {
                var s = source.ToString();
                return int.Parse(s);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static long ToLong(this object source, string remove = ",")
        {
            try
            {
                var s = source.ToString().Replace(remove, "");
                return long.Parse(s);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double ToDouble(this object source, string remove = ",")
        {
            try
            {
                return double.Parse(source.ToString().Replace(remove, ""));
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static bool ToBool(this object source)
        {
            try
            {
                var s = source.ToString();
                return bool.Parse(s);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static DateTime ToDateTime(this object source, params string[] formats)
        {
            if (source == null) return DateTime.MinValue;
            try
            {
                if (formats.Length < 1)
                {
                    formats = new[]
                    {
                        "MM/dd/yyyy hh:mm:ss",
                        "dd/MM/yyyy hh:mm:ss",
                        "dd/MM/yyyy",
                        "MM/dd/yyyy",
                        "dd/M/yyyy",
                        "dd-MM-yyyy",
                        "MM-dd-yyyy",
                        "yyyy-MM-dd",
                        "yyyy/MM/dd",
                        "yyyy/dd/MM",
                        "MM-dd-yyyy hh:mm:ss",
                        "dd-MM-yyyy hh:mm:ss"
                    };
                }
                DateTime result;
                if (DateTime.TryParseExact(source.ToString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                {
                    return result;
                }
            }
            catch
            {
                return DateTime.MinValue;
            }
            return DateTime.MinValue;
        }
        public static string[] GetSplit(this string source, char comma)
        {
            return string.IsNullOrEmpty(source) ? new string[0] : source.Split(comma).Where(i => !string.IsNullOrEmpty(i)).ToArray();
        }
        public static int[] GetInts(this string source, char comma)
        {
            try
            {
                return source.Split(comma).Where(i => !string.IsNullOrEmpty(i)).Select(int.Parse).ToArray();
            }
            catch
            {
                return new int[0];
            }
        }
        public static string JoinArray(this IEnumerable<object> source, char comma)
        {
            if (source == null) return "";
            var r = source.Where(i => i != null).Aggregate("", (c, i) => c + comma + i);
            return r.Length > 1 ? r.Substring(1) : "";
        }
        public static string JoinArray(this IEnumerable<object> source, string comma)
        {
            if (source == null) return "";
            var r = source.Aggregate("", (c, i) => c + comma + i);
            return r.Length > 1 ? r.Substring(comma.Length) : "";
        }
        public static string JoinArray(this int[] source, char comma)
        {
            if (source == null) return "";
            var r = source.Aggregate("", (c, i) => c + comma + i);
            return r.Length > 1 ? r.Substring(1) : "";
        }
        public static bool ContainSplit(this string source, string term, char split = ' ')
        {
            if (string.IsNullOrEmpty(source)) return false;
            var k = term.ToLower().Split(split);
            source = source.ToLower().Replace("-", "");
            return k.Any(s => source.Contains(s));
        }
        public static string ConvertToUnSign(string input)
        {
            input = input.Trim();
            for (int i = 0x20; i < 0x30; i++)
            {
                input = input.Replace(((char)i).ToString(), " ");
            }
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string str = input.Normalize(NormalizationForm.FormD);
            string str2 = regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
            while (str2.IndexOf("?") >= 0)
            {
                str2 = str2.Remove(str2.IndexOf("?"), 1);
            }
            return str2;
        }
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(48, 57)));
                sb.Append(c);
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 90)));
                sb.Append(c);
                c = Convert.ToChar(Convert.ToInt32(rand.Next(97, 122)));
                sb.Append(c);
            }

            if (lowerCase)
            {
                return sb.ToString().ToLower();
            }
            return sb.ToString();
        }
        //public static string RandomStringGenerator(int lengcode,bool lowerCase)
        //{
        //    var Str= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefjhijklmnopqrstuvwxyz0123456789";
        //    StringBuilder sb = new StringBuilder();
        //    int lencode = Str.Length;
        //    for (int i =0; )

        //}
    }
}
