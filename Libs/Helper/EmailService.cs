﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Core.Helper
{
    public class EmailService
    {
        private string _host, _username, _password;
        private int _port;
        private MailAddress _from;
        public void Config(string host, int port, string username, string password, string senderName, string senderEmail)
        {
            _host = host;
            _port = port;
            _username = username;
            _password = password;
            _from = new MailAddress(senderEmail, senderName, Encoding.Unicode);
        }

        private delegate void EmailSender(string subject, string body, IList<string> to, IList<string> cc = null, IList<string> bcc = null);

        public void SendAsync(string subject, string body, IList<string> to, IList<string> cc = null, IList<string> bcc = null)
        {
            EmailSender s = SendEmail;
            s.BeginInvoke(subject, body, to, cc, bcc, null, null);
        }

        public void SendEmail(string subject, string body, IList<string> to, IList<string> cc = null, IList<string> bcc = null)
        {
            var c = new SmtpClient(_host, _port)
            {
                Credentials = new NetworkCredential(_username, _password),
                EnableSsl = true
            };
            var mail = new MailMessage
            {
                Body = body,
                Subject = subject,
                IsBodyHtml = true,
                From = _from
            };
            foreach (var r in to)
            {
                mail.To.Add(new MailAddress(r));
            }
            if (cc != null)
            {
                foreach (var r in cc) mail.CC.Add(new MailAddress(r));
            }
            if (bcc != null)
            {
                foreach (var r in bcc) mail.Bcc.Add(new MailAddress(r));
            }
            c.Send(mail);
        }
    }
}
