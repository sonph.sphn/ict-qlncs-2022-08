﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace Core.Helper
{
    public static class Common
    {
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
        public static void EachMonth(DateTime start, DateTime end, Action<DateTime> action)
        {
            const string format = "yyyy-MM";
            var startDt = DateTime.ParseExact(start.ToString(format), format, CultureInfo.InvariantCulture);
            var endDt = DateTime.ParseExact(end.ToString(format), format, CultureInfo.InvariantCulture);
            while (startDt <= endDt)
            {
                action(startDt);
                startDt = startDt.AddMonths(1);
            }
        }
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
        public static char GetNextAlphabetCharacter(char current)
        {
            return (char)(current + 1);
        }
        public static string GetVisitorIp()
        {
            string ip = null;
            if (HttpContext.Current != null)
            { // ASP.NET
                ip = string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"])
                    ? HttpContext.Current.Request.UserHostAddress
                    : HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            if (!string.IsNullOrEmpty(ip) && ip.Trim() != "::1") return ip; // still can't decide or is LAN
            var lan = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(r => r.AddressFamily == AddressFamily.InterNetwork);
            ip = lan?.ToString() ?? string.Empty;
            return ip;
        }
        public static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (byteCount == 0) return "0" + suf[0];
            var bytes = Math.Abs(byteCount);
            var place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            var num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString(CultureInfo.InvariantCulture) + suf[place];
        }
        public static string RemoveNonAlphaNumberic(string input)
        {
            var rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(input, "");
        }
        public static string RemoveNonAlphaNumbericDot(string input)
        {
            var rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(input, "");
        }
        public static string GetUniqueFilename(string input)
        {
            var rgx = new Regex("[^a-zA-Z0-9 -._]");
            input = input.Replace(" ", "_");
            input = rgx.Replace(input, "");
            return GetTimeString() + "-" + input;
        }
        public static string GetAlias(string title)
        {
            if (string.IsNullOrEmpty(title)) return "";
            title = UnicodeToAscii(title.ToLower()).Trim().Replace(" ", "-");
            while (title.IndexOf("--", StringComparison.Ordinal) > 0)
            {
                title = title.Replace("--", "-");
            }
            return new Regex("[^a-zA-Z0-9 -]").Replace(title, "");
        }
        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException(nameof(extension));
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            string mime;
            return Mappings.TryGetValue(extension, out mime) ? mime : "application/octet-stream";
        }
        private static readonly IDictionary<string, string> Mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
            #region Big freaking list of mime types
            // combination of values from Windows 7 Registry and 
            // from C:\Windows\System32\inetsrv\config\applicationHost.config
            // some added, including .7z and .dat
            {".323", "text/h323"},
        {".3g2", "video/3gpp2"},
        {".3gp", "video/3gpp"},
        {".3gp2", "video/3gpp2"},
        {".3gpp", "video/3gpp"},
        {".7z", "application/x-7z-compressed"},
        {".aa", "audio/audible"},
        {".AAC", "audio/aac"},
        {".aaf", "application/octet-stream"},
        {".aax", "audio/vnd.audible.aax"},
        {".ac3", "audio/ac3"},
        {".aca", "application/octet-stream"},
        {".accda", "application/msaccess.addin"},
        {".accdb", "application/msaccess"},
        {".accdc", "application/msaccess.cab"},
        {".accde", "application/msaccess"},
        {".accdr", "application/msaccess.runtime"},
        {".accdt", "application/msaccess"},
        {".accdw", "application/msaccess.webapplication"},
        {".accft", "application/msaccess.ftemplate"},
        {".acx", "application/internet-property-stream"},
        {".AddIn", "text/xml"},
        {".ade", "application/msaccess"},
        {".adobebridge", "application/x-bridge-url"},
        {".adp", "application/msaccess"},
        {".ADT", "audio/vnd.dlna.adts"},
        {".ADTS", "audio/aac"},
        {".afm", "application/octet-stream"},
        {".ai", "application/postscript"},
        {".aif", "audio/x-aiff"},
        {".aifc", "audio/aiff"},
        {".aiff", "audio/aiff"},
        {".air", "application/vnd.adobe.air-application-installer-package+zip"},
        {".amc", "application/x-mpeg"},
        {".application", "application/x-ms-application"},
        {".art", "image/x-jg"},
        {".asa", "application/xml"},
        {".asax", "application/xml"},
        {".ascx", "application/xml"},
        {".asd", "application/octet-stream"},
        {".asf", "video/x-ms-asf"},
        {".ashx", "application/xml"},
        {".asi", "application/octet-stream"},
        {".asm", "text/plain"},
        {".asmx", "application/xml"},
        {".aspx", "application/xml"},
        {".asr", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".atom", "application/atom+xml"},
        {".au", "audio/basic"},
        {".avi", "video/x-msvideo"},
        {".axs", "application/olescript"},
        {".bas", "text/plain"},
        {".bcpio", "application/x-bcpio"},
        {".bin", "application/octet-stream"},
        {".bmp", "image/bmp"},
        {".c", "text/plain"},
        {".cab", "application/octet-stream"},
        {".caf", "audio/x-caf"},
        {".calx", "application/vnd.ms-office.calx"},
        {".cat", "application/vnd.ms-pki.seccat"},
        {".cc", "text/plain"},
        {".cd", "text/plain"},
        {".cdda", "audio/aiff"},
        {".cdf", "application/x-cdf"},
        {".cer", "application/x-x509-ca-cert"},
        {".chm", "application/octet-stream"},
        {".class", "application/x-java-applet"},
        {".clp", "application/x-msclip"},
        {".cmx", "image/x-cmx"},
        {".cnf", "text/plain"},
        {".cod", "image/cis-cod"},
        {".config", "application/xml"},
        {".contact", "text/x-ms-contact"},
        {".coverage", "application/xml"},
        {".cpio", "application/x-cpio"},
        {".cpp", "text/plain"},
        {".crd", "application/x-mscardfile"},
        {".crl", "application/pkix-crl"},
        {".crt", "application/x-x509-ca-cert"},
        {".cs", "text/plain"},
        {".csdproj", "text/plain"},
        {".csh", "application/x-csh"},
        {".csproj", "text/plain"},
        {".css", "text/css"},
        {".csv", "application/octet-stream"},
        {".cur", "application/octet-stream"},
        {".cxx", "text/plain"},
        {".dat", "application/octet-stream"},
        {".datasource", "application/xml"},
        {".dbproj", "text/plain"},
        {".dcr", "application/x-director"},
        {".def", "text/plain"},
        {".deploy", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dgml", "application/xml"},
        {".dib", "image/bmp"},
        {".dif", "video/x-dv"},
        {".dir", "application/x-director"},
        {".disco", "text/xml"},
        {".dll", "application/x-msdownload"},
        {".dll.config", "text/xml"},
        {".dlm", "text/dlm"},
        {".doc", "application/msword"},
        {".docm", "application/vnd.ms-word.document.macroEnabled.12"},
        {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
        {".dot", "application/msword"},
        {".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
        {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
        {".dsp", "application/octet-stream"},
        {".dsw", "text/plain"},
        {".dtd", "text/xml"},
        {".dtsConfig", "text/xml"},
        {".dv", "video/x-dv"},
        {".dvi", "application/x-dvi"},
        {".dwf", "drawing/x-dwf"},
        {".dwp", "application/octet-stream"},
        {".dxr", "application/x-director"},
        {".eml", "message/rfc822"},
        {".emz", "application/octet-stream"},
        {".eot", "application/octet-stream"},
        {".eps", "application/postscript"},
        {".etl", "application/etl"},
        {".etx", "text/x-setext"},
        {".evy", "application/envoy"},
        {".exe", "application/octet-stream"},
        {".exe.config", "text/xml"},
        {".fdf", "application/vnd.fdf"},
        {".fif", "application/fractals"},
        {".filters", "Application/xml"},
        {".fla", "application/octet-stream"},
        {".flr", "x-world/x-vrml"},
        {".flv", "video/x-flv"},
        {".fsscript", "application/fsharp-script"},
        {".fsx", "application/fsharp-script"},
        {".generictest", "application/xml"},
        {".gif", "image/gif"},
        {".group", "text/x-ms-group"},
        {".gsm", "audio/x-gsm"},
        {".gtar", "application/x-gtar"},
        {".gz", "application/x-gzip"},
        {".h", "text/plain"},
        {".hdf", "application/x-hdf"},
        {".hdml", "text/x-hdml"},
        {".hhc", "application/x-oleobject"},
        {".hhk", "application/octet-stream"},
        {".hhp", "application/octet-stream"},
        {".hlp", "application/winhlp"},
        {".hpp", "text/plain"},
        {".hqx", "application/mac-binhex40"},
        {".hta", "application/hta"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".htt", "text/webviewhtml"},
        {".hxa", "application/xml"},
        {".hxc", "application/xml"},
        {".hxd", "application/octet-stream"},
        {".hxe", "application/xml"},
        {".hxf", "application/xml"},
        {".hxh", "application/octet-stream"},
        {".hxi", "application/octet-stream"},
        {".hxk", "application/xml"},
        {".hxq", "application/octet-stream"},
        {".hxr", "application/octet-stream"},
        {".hxs", "application/octet-stream"},
        {".hxt", "text/html"},
        {".hxv", "application/xml"},
        {".hxw", "application/octet-stream"},
        {".hxx", "text/plain"},
        {".i", "text/plain"},
        {".ico", "image/x-icon"},
        {".ics", "application/octet-stream"},
        {".idl", "text/plain"},
        {".ief", "image/ief"},
        {".iii", "application/x-iphone"},
        {".inc", "text/plain"},
        {".inf", "application/octet-stream"},
        {".inl", "text/plain"},
        {".ins", "application/x-internet-signup"},
        {".ipa", "application/x-itunes-ipa"},
        {".ipg", "application/x-itunes-ipg"},
        {".ipproj", "text/plain"},
        {".ipsw", "application/x-itunes-ipsw"},
        {".iqy", "text/x-ms-iqy"},
        {".isp", "application/x-internet-signup"},
        {".ite", "application/x-itunes-ite"},
        {".itlp", "application/x-itunes-itlp"},
        {".itms", "application/x-itunes-itms"},
        {".itpc", "application/x-itunes-itpc"},
        {".IVF", "video/x-ivf"},
        {".jar", "application/java-archive"},
        {".java", "application/octet-stream"},
        {".jck", "application/liquidmotion"},
        {".jcz", "application/liquidmotion"},
        {".jfif", "image/pjpeg"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpb", "application/octet-stream"},
        {".jpe", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".jsx", "text/jscript"},
        {".jsxbin", "text/plain"},
        {".latex", "application/x-latex"},
        {".library-ms", "application/windows-library+xml"},
        {".lit", "application/x-ms-reader"},
        {".loadtest", "application/xml"},
        {".lpk", "application/octet-stream"},
        {".lsf", "video/x-la-asf"},
        {".lst", "text/plain"},
        {".lsx", "video/x-la-asf"},
        {".lzh", "application/octet-stream"},
        {".m13", "application/x-msmediaview"},
        {".m14", "application/x-msmediaview"},
        {".m1v", "video/mpeg"},
        {".m2t", "video/vnd.dlna.mpeg-tts"},
        {".m2ts", "video/vnd.dlna.mpeg-tts"},
        {".m2v", "video/mpeg"},
        {".m3u", "audio/x-mpegurl"},
        {".m3u8", "audio/x-mpegurl"},
        {".m4a", "audio/m4a"},
        {".m4b", "audio/m4b"},
        {".m4p", "audio/m4p"},
        {".m4r", "audio/x-m4r"},
        {".m4v", "video/x-m4v"},
        {".mac", "image/x-macpaint"},
        {".mak", "text/plain"},
        {".man", "application/x-troff-man"},
        {".manifest", "application/x-ms-manifest"},
        {".map", "text/plain"},
        {".master", "application/xml"},
        {".mda", "application/msaccess"},
        {".mdb", "application/x-msaccess"},
        {".mde", "application/msaccess"},
        {".mdp", "application/octet-stream"},
        {".me", "application/x-troff-me"},
        {".mfp", "application/x-shockwave-flash"},
        {".mht", "message/rfc822"},
        {".mhtml", "message/rfc822"},
        {".mid", "audio/mid"},
        {".midi", "audio/mid"},
        {".mix", "application/octet-stream"},
        {".mk", "text/plain"},
        {".mmf", "application/x-smaf"},
        {".mno", "text/xml"},
        {".mny", "application/x-msmoney"},
        {".mod", "video/mpeg"},
        {".mov", "video/quicktime"},
        {".movie", "video/x-sgi-movie"},
        {".mp2", "video/mpeg"},
        {".mp2v", "video/mpeg"},
        {".mp3", "audio/mpeg"},
        {".mp4", "video/mp4"},
        {".mp4v", "video/mp4"},
        {".mpa", "video/mpeg"},
        {".mpe", "video/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpf", "application/vnd.ms-mediapackage"},
        {".mpg", "video/mpeg"},
        {".mpp", "application/vnd.ms-project"},
        {".mpv2", "video/mpeg"},
        {".mqv", "video/quicktime"},
        {".ms", "application/x-troff-ms"},
        {".msi", "application/octet-stream"},
        {".mso", "application/octet-stream"},
        {".mts", "video/vnd.dlna.mpeg-tts"},
        {".mtx", "application/xml"},
        {".mvb", "application/x-msmediaview"},
        {".mvc", "application/x-miva-compiled"},
        {".mxp", "application/x-mmxp"},
        {".nc", "application/x-netcdf"},
        {".nsc", "video/x-ms-asf"},
        {".nws", "message/rfc822"},
        {".ocx", "application/octet-stream"},
        {".oda", "application/oda"},
        {".odc", "text/x-ms-odc"},
        {".odh", "text/plain"},
        {".odl", "text/plain"},
        {".odp", "application/vnd.oasis.opendocument.presentation"},
        {".ods", "application/oleobject"},
        {".odt", "application/vnd.oasis.opendocument.text"},
        {".one", "application/onenote"},
        {".onea", "application/onenote"},
        {".onepkg", "application/onenote"},
        {".onetmp", "application/onenote"},
        {".onetoc", "application/onenote"},
        {".onetoc2", "application/onenote"},
        {".orderedtest", "application/xml"},
        {".osdx", "application/opensearchdescription+xml"},
        {".p10", "application/pkcs10"},
        {".p12", "application/x-pkcs12"},
        {".p7b", "application/x-pkcs7-certificates"},
        {".p7c", "application/pkcs7-mime"},
        {".p7m", "application/pkcs7-mime"},
        {".p7r", "application/x-pkcs7-certreqresp"},
        {".p7s", "application/pkcs7-signature"},
        {".pbm", "image/x-portable-bitmap"},
        {".pcast", "application/x-podcast"},
        {".pct", "image/pict"},
        {".pcx", "application/octet-stream"},
        {".pcz", "application/octet-stream"},
        {".pdf", "application/pdf"},
        {".pfb", "application/octet-stream"},
        {".pfm", "application/octet-stream"},
        {".pfx", "application/x-pkcs12"},
        {".pgm", "image/x-portable-graymap"},
        {".pic", "image/pict"},
        {".pict", "image/pict"},
        {".pkgdef", "text/plain"},
        {".pkgundef", "text/plain"},
        {".pko", "application/vnd.ms-pki.pko"},
        {".pls", "audio/scpls"},
        {".pma", "application/x-perfmon"},
        {".pmc", "application/x-perfmon"},
        {".pml", "application/x-perfmon"},
        {".pmr", "application/x-perfmon"},
        {".pmw", "application/x-perfmon"},
        {".png", "image/png"},
        {".pnm", "image/x-portable-anymap"},
        {".pnt", "image/x-macpaint"},
        {".pntg", "image/x-macpaint"},
        {".pnz", "image/png"},
        {".pot", "application/vnd.ms-powerpoint"},
        {".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
        {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
        {".ppa", "application/vnd.ms-powerpoint"},
        {".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
        {".ppm", "image/x-portable-pixmap"},
        {".pps", "application/vnd.ms-powerpoint"},
        {".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
        {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
        {".ppt", "application/vnd.ms-powerpoint"},
        {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
        {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
        {".prf", "application/pics-rules"},
        {".prm", "application/octet-stream"},
        {".prx", "application/octet-stream"},
        {".ps", "application/postscript"},
        {".psc1", "application/PowerShell"},
        {".psd", "application/octet-stream"},
        {".psess", "application/xml"},
        {".psm", "application/octet-stream"},
        {".psp", "application/octet-stream"},
        {".pub", "application/x-mspublisher"},
        {".pwz", "application/vnd.ms-powerpoint"},
        {".qht", "text/x-html-insertion"},
        {".qhtm", "text/x-html-insertion"},
        {".qt", "video/quicktime"},
        {".qti", "image/x-quicktime"},
        {".qtif", "image/x-quicktime"},
        {".qtl", "application/x-quicktimeplayer"},
        {".qxd", "application/octet-stream"},
        {".ra", "audio/x-pn-realaudio"},
        {".ram", "audio/x-pn-realaudio"},
        {".rar", "application/octet-stream"},
        {".ras", "image/x-cmu-raster"},
        {".rat", "application/rat-file"},
        {".rc", "text/plain"},
        {".rc2", "text/plain"},
        {".rct", "text/plain"},
        {".rdlc", "application/xml"},
        {".resx", "application/xml"},
        {".rf", "image/vnd.rn-realflash"},
        {".rgb", "image/x-rgb"},
        {".rgs", "text/plain"},
        {".rm", "application/vnd.rn-realmedia"},
        {".rmi", "audio/mid"},
        {".rmp", "application/vnd.rn-rn_music_package"},
        {".roff", "application/x-troff"},
        {".rpm", "audio/x-pn-realaudio-plugin"},
        {".rqy", "text/x-ms-rqy"},
        {".rtf", "application/rtf"},
        {".rtx", "text/richtext"},
        {".ruleset", "application/xml"},
        {".s", "text/plain"},
        {".safariextz", "application/x-safari-safariextz"},
        {".scd", "application/x-msschedule"},
        {".sct", "text/scriptlet"},
        {".sd2", "audio/x-sd2"},
        {".sdp", "application/sdp"},
        {".sea", "application/octet-stream"},
        {".searchConnector-ms", "application/windows-search-connector+xml"},
        {".setpay", "application/set-payment-initiation"},
        {".setreg", "application/set-registration-initiation"},
        {".settings", "application/xml"},
        {".sgimb", "application/x-sgimb"},
        {".sgml", "text/sgml"},
        {".sh", "application/x-sh"},
        {".shar", "application/x-shar"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".sitemap", "application/xml"},
        {".skin", "application/xml"},
        {".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
        {".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
        {".slk", "application/vnd.ms-excel"},
        {".sln", "text/plain"},
        {".slupkg-ms", "application/x-ms-license"},
        {".smd", "audio/x-smd"},
        {".smi", "application/octet-stream"},
        {".smx", "audio/x-smd"},
        {".smz", "audio/x-smd"},
        {".snd", "audio/basic"},
        {".snippet", "application/xml"},
        {".snp", "application/octet-stream"},
        {".sol", "text/plain"},
        {".sor", "text/plain"},
        {".spc", "application/x-pkcs7-certificates"},
        {".spl", "application/futuresplash"},
        {".src", "application/x-wais-source"},
        {".srf", "text/plain"},
        {".SSISDeploymentManifest", "text/xml"},
        {".ssm", "application/streamingmedia"},
        {".sst", "application/vnd.ms-pki.certstore"},
        {".stl", "application/vnd.ms-pki.stl"},
        {".sv4cpio", "application/x-sv4cpio"},
        {".sv4crc", "application/x-sv4crc"},
        {".svc", "application/xml"},
        {".swf", "application/x-shockwave-flash"},
        {".t", "application/x-troff"},
        {".tar", "application/x-tar"},
        {".tcl", "application/x-tcl"},
        {".testrunconfig", "application/xml"},
        {".testsettings", "application/xml"},
        {".tex", "application/x-tex"},
        {".texi", "application/x-texinfo"},
        {".texinfo", "application/x-texinfo"},
        {".tgz", "application/x-compressed"},
        {".thmx", "application/vnd.ms-officetheme"},
        {".thn", "application/octet-stream"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".tlh", "text/plain"},
        {".tli", "text/plain"},
        {".toc", "application/octet-stream"},
        {".tr", "application/x-troff"},
        {".trm", "application/x-msterminal"},
        {".trx", "application/xml"},
        {".ts", "video/vnd.dlna.mpeg-tts"},
        {".tsv", "text/tab-separated-values"},
        {".ttf", "application/octet-stream"},
        {".tts", "video/vnd.dlna.mpeg-tts"},
        {".txt", "text/plain"},
        {".u32", "application/octet-stream"},
        {".uls", "text/iuls"},
        {".user", "text/plain"},
        {".ustar", "application/x-ustar"},
        {".vb", "text/plain"},
        {".vbdproj", "text/plain"},
        {".vbk", "video/mpeg"},
        {".vbproj", "text/plain"},
        {".vbs", "text/vbscript"},
        {".vcf", "text/x-vcard"},
        {".vcproj", "Application/xml"},
        {".vcs", "text/plain"},
        {".vcxproj", "Application/xml"},
        {".vddproj", "text/plain"},
        {".vdp", "text/plain"},
        {".vdproj", "text/plain"},
        {".vdx", "application/vnd.ms-visio.viewer"},
        {".vml", "text/xml"},
        {".vscontent", "application/xml"},
        {".vsct", "text/xml"},
        {".vsd", "application/vnd.visio"},
        {".vsi", "application/ms-vsi"},
        {".vsix", "application/vsix"},
        {".vsixlangpack", "text/xml"},
        {".vsixmanifest", "text/xml"},
        {".vsmdi", "application/xml"},
        {".vspscc", "text/plain"},
        {".vss", "application/vnd.visio"},
        {".vsscc", "text/plain"},
        {".vssettings", "text/xml"},
        {".vssscc", "text/plain"},
        {".vst", "application/vnd.visio"},
        {".vstemplate", "text/xml"},
        {".vsto", "application/x-ms-vsto"},
        {".vsw", "application/vnd.visio"},
        {".vsx", "application/vnd.visio"},
        {".vtx", "application/vnd.visio"},
        {".wav", "audio/wav"},
        {".wave", "audio/wav"},
        {".wax", "audio/x-ms-wax"},
        {".wbk", "application/msword"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wcm", "application/vnd.ms-works"},
        {".wdb", "application/vnd.ms-works"},
        {".wdp", "image/vnd.ms-photo"},
        {".webarchive", "application/x-safari-webarchive"},
        {".webtest", "application/xml"},
        {".wiq", "application/xml"},
        {".wiz", "application/msword"},
        {".wks", "application/vnd.ms-works"},
        {".WLMP", "application/wlmoviemaker"},
        {".wlpginstall", "application/x-wlpg-detect"},
        {".wlpginstall3", "application/x-wlpg3-detect"},
        {".wm", "video/x-ms-wm"},
        {".wma", "audio/x-ms-wma"},
        {".wmd", "application/x-ms-wmd"},
        {".wmf", "application/x-msmetafile"},
        {".wml", "text/vnd.wap.wml"},
        {".wmlc", "application/vnd.wap.wmlc"},
        {".wmls", "text/vnd.wap.wmlscript"},
        {".wmlsc", "application/vnd.wap.wmlscriptc"},
        {".wmp", "video/x-ms-wmp"},
        {".wmv", "video/x-ms-wmv"},
        {".wmx", "video/x-ms-wmx"},
        {".wmz", "application/x-ms-wmz"},
        {".wpl", "application/vnd.ms-wpl"},
        {".wps", "application/vnd.ms-works"},
        {".wri", "application/x-mswrite"},
        {".wrl", "x-world/x-vrml"},
        {".wrz", "x-world/x-vrml"},
        {".wsc", "text/scriptlet"},
        {".wsdl", "text/xml"},
        {".wvx", "video/x-ms-wvx"},
        {".x", "application/directx"},
        {".xaf", "x-world/x-vrml"},
        {".xaml", "application/xaml+xml"},
        {".xap", "application/x-silverlight-app"},
        {".xbap", "application/x-ms-xbap"},
        {".xbm", "image/x-xbitmap"},
        {".xdr", "text/plain"},
        {".xht", "application/xhtml+xml"},
        {".xhtml", "application/xhtml+xml"},
        {".xla", "application/vnd.ms-excel"},
        {".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
        {".xlc", "application/vnd.ms-excel"},
        {".xld", "application/vnd.ms-excel"},
        {".xlk", "application/vnd.ms-excel"},
        {".xll", "application/vnd.ms-excel"},
        {".xlm", "application/vnd.ms-excel"},
        {".xls", "application/vnd.ms-excel"},
        {".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
        {".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        {".xlt", "application/vnd.ms-excel"},
        {".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
        {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
        {".xlw", "application/vnd.ms-excel"},
        {".xml", "text/xml"},
        {".xmta", "application/xml"},
        {".xof", "x-world/x-vrml"},
        {".XOML", "text/plain"},
        {".xpm", "image/x-xpixmap"},
        {".xps", "application/vnd.ms-xpsdocument"},
        {".xrm-ms", "text/xml"},
        {".xsc", "application/xml"},
        {".xsd", "text/xml"},
        {".xsf", "text/xml"},
        {".xsl", "text/xml"},
        {".xslt", "text/xml"},
        {".xsn", "application/octet-stream"},
        {".xss", "application/xml"},
        {".xtp", "application/octet-stream"},
        {".xwd", "image/x-xwindowdump"},
        {".z", "application/x-compress"},
        {".zip", "application/x-zip-compressed"},
            #endregion
        };
        public static string FirstUpper(this string input)
        {
            if (string.IsNullOrEmpty(input)) return "";
            return input.First().ToString().ToUpper() + string.Join("", input.Skip(1));
        }
        public static string CleanStringOfNonDigits(string s)
        {
            return string.IsNullOrEmpty(s) ? s : new Regex(@"[^\d]+").Replace(s, "");
        }
        public static string ToNumberString(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            var len = number.Length;
            number += "ss";
            var doc = "";
            var rd = 0;
            var i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                var n = (len - i + 2) % 3 + 1;
                //Kiem tra so 0
                var found = 0;
                int j;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] == '0') continue;
                    found = 1;
                    break;
                }
                //Duyet n chu so
                int k;
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        var ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[number[i + j] - 48] + " ";
                                break;
                        }
                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }
                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++) doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }
            if (len != 1) return doc;
            if (number[0] == '0' || number[0] == '5') return cs[number[0] - 48];
            return doc;
        }
        public static string ToDecimalCode(string str)
        {
            var outputString = "";
            var haut = 0;
            foreach (var b in str)
            {
                if (b < 0 || b > 0xFFFF)
                {
                    outputString += "!error " + Dec2HexCode(b) + "!";
                }
                if (haut != 0)
                {
                    if (0xDC00 <= b && b <= 0xDFFF)
                    {
                        outputString += Dec2HexCode(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + "";
                        haut = 0;
                        continue;
                    }
                    outputString += "!error " + Dec2HexCode(haut) + "!";
                    haut = 0;
                }
                if (0xD800 <= b && b <= 0xDBFF)
                {
                    haut = b;
                }
                else
                {
                    outputString += Dec2HexCode(b) + "";
                }
            }
            return outputString;
        }
        private static string Dec2HexCode(int textString)
        {
            var t = textString + 0;
            var temp = t.ToString().ToUpper() + " ";
            return temp;
        }
        public static string ToDecimalNcr(string str)
        {
            var outputString = "";
            var haut = 0;
            foreach (var b in str)
            {
                if (b < 0 || b > 0xFFFF)
                {
                    outputString += "!error " + Dec2Hex(b) + "!";
                }
                if (haut != 0)
                {
                    if (0xDC00 <= b && b <= 0xDFFF)
                    {
                        outputString += Dec2Hex(0x10000 + ((haut - 0xD800) << 10) + (b - 0xDC00)) + "";
                        haut = 0;
                        continue;
                    }
                    outputString += "!error " + Dec2Hex(haut) + "!";
                    haut = 0;
                }
                if (0xD800 <= b && b <= 0xDBFF)
                {
                    haut = b;
                }
                else
                {
                    outputString += Dec2Hex(b) + "";
                }
            }
            return outputString;
        }
        private static string Dec2Hex(int textString)
        {
            var t = textString + 0;
            var temp = "&#" + t.ToString().ToUpper() + ";";
            return temp.Trim();
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (var i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            return new string(stringChars);
        }
        public static string GetSplitFormat(string number, int splitlenght = 3, string splitCharacter = ".")
        {
            var k = 0;
            var result = "";
            for (var i = number.Length - 1; i >= 0; i--)
            {
                result += number[i];
                k++;
                if (k != splitlenght) continue;
                k = 0;
                if (i > 0) result += splitCharacter;
            }
            return result.Reverse().Aggregate("", (c, i) => c + i);
        }
        public static bool IsImage(string filename)
        {
            var ietx = new[] { ".jpg", ".png", ".bmp", ".gif", ".tiff", ".tif" };
            return filename != null && ietx.Contains(Path.GetExtension(filename).ToLower());
        }
        public static bool IsVideo(string filename)
        {
            var exs = new[] { ".mp4", ".mov", ".flv", ".mpg", ".ogg", ".webm", ".wmv" };
            return filename != null && exs.Contains(Path.GetExtension(filename).ToLower());
        }
        public static void DeleteFile(string relativeFilePath)
        {
            try
            {
                var path = AppDomain.CurrentDomain.BaseDirectory + relativeFilePath;
                if (File.Exists(path)) File.Delete(path);
            }
            catch { }
        }
        public static void DeleteFolder(string relativePath)
        {
            try
            {
                var path = AppDomain.CurrentDomain.BaseDirectory + relativePath;
                if (Directory.Exists(path)) Directory.Delete(path, true);
            }
            catch { }
        }
        public static string GetPhysicalPath(string relativePath)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + relativePath;
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            return path;
        }
        public static Image ResizeImage(Stream image, Size size)
        {
            return ResizeImage(Image.FromStream(image), size);
        }
        public static Image CropImage(Stream image, int x, int y, int w, int h)
        {
            return CropImage(Image.FromStream(image), x, y, w, h);
        }
        public static Image ResizeImage(Image image, Size size)
        {
            Image thumbnail = new Bitmap(size.Width, size.Height, image.PixelFormat);
            var oGraphic = Graphics.FromImage(thumbnail);
            oGraphic.CompositingQuality = CompositingQuality.HighQuality;
            oGraphic.SmoothingMode = SmoothingMode.HighQuality;
            oGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var oRectangle = new Rectangle(0, 0, size.Width, size.Height);
            oGraphic.DrawImage(image, oRectangle);
            return thumbnail;
        }
        public static Image CropImage(Image image, int x, int y, int w, int h)
        {
            Image thumbnail = new Bitmap(w, h, image.PixelFormat);
            var oGraphic = Graphics.FromImage(thumbnail);
            oGraphic.CompositingQuality = CompositingQuality.HighQuality;
            oGraphic.SmoothingMode = SmoothingMode.HighQuality;
            oGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var oRectangle = new Rectangle(0, 0, w, h);
            oGraphic.DrawImage(image, oRectangle, x, y, w, h, GraphicsUnit.Pixel);
            return thumbnail;
        }
        public static Image FixedSize(Stream image, Size size)
        {
            return FixedSize(Image.FromStream(image), size.Width, size.Height);
        }

        public static Image FixedSize(Image image, int width, int height)
        {
            var sourceWidth = image.Width;
            var sourceHeight = image.Height;
            const int sourceX = 0;
            const int sourceY = 0;

            var nScaleW = (width / (double)sourceWidth);
            var nScaleH = (height / (double)sourceHeight);

            var nScale = Math.Max(nScaleH, nScaleW);
            var destY = (height - sourceHeight * nScale) / 2;
            var destX = (width - sourceWidth * nScale) / 2;

            var destWidth = (int)Math.Round(sourceWidth * nScale);
            var destHeight = (int)Math.Round(sourceHeight * nScale);

            var bmPhoto = new Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            using (var grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                var to = new Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                var from = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, GraphicsUnit.Pixel);
                return bmPhoto;
            }
        }

        public static Image FixedSizeTopLeft(Stream image, Size size)
        {
            return FixedSizeTopLeft(Image.FromStream(image), size.Width, size.Height);
        }

        public static Image FixedSizeTopLeft(Image image, int width, int height)
        {
            var sourceWidth = image.Width;
            var sourceHeight = image.Height;
            const int sourceX = 0;
            const int sourceY = 0;

            var nScaleW = (width / (double)sourceWidth);
            var nScaleH = (height / (double)sourceHeight);

            var nScale = Math.Max(nScaleH, nScaleW);
            var destY = (height - sourceHeight * nScale) / 2;
            var destX = (width - sourceWidth * nScale) / 2;

            var destWidth = (int)Math.Round(sourceWidth * nScale);
            var destHeight = (int)Math.Round(sourceHeight * nScale);

            var bmPhoto = new Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            using (var grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                var to = new Rectangle(0, 0, destWidth, destHeight);
                var from = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, GraphicsUnit.Pixel);
                return bmPhoto;
            }
        }
        public static Image ScaleByWidth(Stream image, int width)
        {
            var img = Image.FromStream(image);
            var height = width * img.Height / img.Width;
            return ResizeImage(image, new Size(width, height));
        }
        public static Image ScaleByHeight(Stream image, int height)
        {
            var img = Image.FromStream(image);
            var width = height * img.Width / img.Height;
            return ResizeImage(image, new Size(width, height));
        }
        public static int BuildTotalPage(int pagesize, int total)
        {
            var totalpage = total / pagesize;
            if (total % pagesize != 0) totalpage++;
            return totalpage;
        }
        public static bool IsValidEmail(string email)
        {
            return !string.IsNullOrEmpty(email) && Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        public static string GetTimeString()
        {
            return HiResDateTime.UtcNowTicks.ToString();
        }
        public static bool IsNumeric(string val, NumberStyles numberStyle)
        {
            Double result;
            return Double.TryParse(val, numberStyle, CultureInfo.CurrentCulture, out result);
        }
        public static string Base64Encode(string data)
        {
            try
            {
                var encDataByte = System.Text.Encoding.UTF8.GetBytes(data);
                var encodedData = Convert.ToBase64String(encDataByte);
                return encodedData.Replace("+", "-").Replace("/", "_");
            }
            catch
            {
                return "";
            }
        }
        public static string Base64Decode(string data)
        {
            try
            {
                var encoder = new System.Text.UTF8Encoding();
                var utf8Decode = encoder.GetDecoder();
                data = data.Replace("-", "+").Replace("_", "/");
                var todecodeByte = Convert.FromBase64String(data);
                var charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                var decodedChar = new char[charCount];
                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
                var result = new String(decodedChar);
                return result;
            }
            catch
            {
                return "";
            }
        }
        public static string UnicodeToAscii(string strKd)
        {
            var retVal = strKd;
            string[] arKd = { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "d", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "D", "I", "I", "I", "I ", "I ", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y" };
            string[] arUn = { "à", "á", "ả", "ã", "ạ", "ầ", "ấ", "ẩ", "ẫ", "ậ", "â", "ằ", "ắ", "ẳ", "ẵ", "ặ", "ă", "è", "é", "ẻ", "ẽ", "ẹ", "ề", "ế", "ể", "ễ", "ệ", "ê", "đ", "ì", "í", "ỉ", "ĩ", "ị", "ò", "ó", "ỏ", "õ", "ọ", "ồ", "ố", "ổ", "ỗ", "ộ", "ô", "ờ", "ớ", "ở", "ỡ", "ợ", "ơ", "ù", "ú", "ủ", "ũ", "ụ", "ừ", "ứ", "ử", "ữ", "ự", "ư", "ỳ", "ý", "ỷ", "ỹ", "ỵ", "À", "Á", "Ả", "Ã", "Ạ", "Ầ", "Ấ", "Ẩ", "Ẫ", "Ậ", "Â", "Ằ", "Ắ", "Ẳ", "Ẵ", "Ặ", "Ă", "È", "É", "Ẻ", "Ẽ", "Ẹ", "Ề", "Ế", "Ể", "Ễ", "Ệ", "Ê", "Đ", "Ì", "Í", "Ỉ", "Ĩ ", "Ị ", "Ò", "Ó", "Ỏ", "Õ", "Ọ", "Ồ", "Ố", "Ổ", "Ỗ", "Ộ", "Ô", "Ờ", "Ớ", "Ở", "Ỡ", "Ợ", "Ơ", "Ù", "Ú", "Ủ", "Ũ", "Ụ", "Ừ", "Ứ", "Ử", "Ữ", "Ự", "Ư", "Ỳ", "Ý", "Ỷ", "Ỹ", "Ỵ" };
            for (var i = 0; i < arUn.Length; i++) retVal = retVal.Replace(arUn[i], arKd[i]);
            while (retVal.Contains("  ")) retVal = retVal.Replace("  ", " ");
            retVal = retVal.Replace("_", "");
            return retVal;
        }
        public static string UnicodeToAscii(string strKd, string replacespacewith)
        {
            return UnicodeToAscii(strKd).Replace(" ", replacespacewith);
        }
        public static string ExtractString(string source, int length)
        {
            var dest = String.Empty;
            if (length == 0 || source.Length == 0)
                return dest;
            if (source.Length < length)
                dest = source;
            else
            {
                var tmp = source.Substring(0, length);
                var nSub = tmp.Length - 1;
                while (tmp[nSub] != ' ')
                {
                    nSub--;
                    if (nSub == 0) break;
                }
                dest = tmp.Substring(0, nSub) + " ...";
            }
            return dest;
        }
        public static string KeepValidHtmlTags(string htmlContent, string[] validHtmlTags)
        {
            var retStr = htmlContent;

            retStr = validHtmlTags != null ? (from Match m in Regex.Matches(htmlContent, "</?(?<tagname>[a-z][a-z0-9]*)[^<>]*>", RegexOptions.IgnoreCase) let tagName = m.Groups["tagname"].Value where !validHtmlTags.Contains(tagName) select m).Aggregate(retStr, (current, m) => current.Replace(m.Value, string.Empty)) : Regex.Replace(htmlContent, "</?(?<tagname>[a-z][a-z0-9]*)[^<>]*>", string.Empty);

            return retStr;
        }
        public static string RemoveInvalidHtmlTags(string htmlContent, string[] inValidHtmlTags)
        {
            var retStr = htmlContent;

            retStr = inValidHtmlTags != null ? (from Match m in Regex.Matches(htmlContent, "</?(?<tagname>[a-z][a-z0-9]*)[^<>]*>", RegexOptions.IgnoreCase) let tagName = m.Groups["tagname"].Value where inValidHtmlTags.Contains(tagName) select m).Aggregate(retStr, (current, m) => current.Replace(m.Value, string.Empty)) : Regex.Replace(htmlContent, "</?(?<tagname>[a-z][a-z0-9]*)[^<>]*>", string.Empty);

            return retStr;
        }
        public static string CleanWordHtml(string html)
        {
            var sc = new StringCollection
                         {
                             @"<!--(\w|\W)+?-->",
                             @"<title>(\w|\W)+?</title>",
                             @"\s?class=\w+",
                             @"\s+style='[^']+'",
                             @"<(meta|link|/?o:|/?style|/?div|/?st\d|/?head|/?html|body|/?body|/?span|!\[)[^>]*?>",
                             @"(<[^>]+>)+&nbsp;(</\w+>)+",
                             @"\s+v:\w+=""[^""]+""",
                             @"(\n\r){2,}"
                         };
            return sc.Cast<string>().Aggregate(html, (current, s) => Regex.Replace(current, s, "", RegexOptions.IgnoreCase));
        }
        public static string ConvertDateTime04(DateTime dtTime)
        {
            var intDay = dtTime.Day;
            var intMonth = dtTime.Month;
            var intYear = dtTime.Year;
            var strResult = dtTime.Hour.ToString(CultureInfo.InvariantCulture) + ":" + dtTime.Minute.ToString(CultureInfo.InvariantCulture) + " Ngày " + intDay.ToString(CultureInfo.InvariantCulture) + "." + intMonth.ToString(CultureInfo.InvariantCulture) + "." + intYear.ToString(CultureInfo.InvariantCulture);
            return strResult;
        }
        public static string ConvertDateTime06(DateTime dtTime)
        {
            var intDay = dtTime.Day < 10 ? "0" + dtTime.Day : dtTime.Day.ToString(CultureInfo.InvariantCulture);
            var intMonth = dtTime.Month < 10 ? "0" + dtTime.Month : dtTime.Month.ToString(CultureInfo.InvariantCulture);
            var intYear = dtTime.Year;
            var strResult = intDay + "/" + intMonth + "/" + intYear.ToString(CultureInfo.InvariantCulture);
            return strResult;
        }
        public static DateTime GetDate(object obj)
        {
            DateTime retVal;
            try
            {
                retVal = Convert.ToDateTime(obj);
            }
            catch
            {
                retVal = DateTime.Now;
            }
            return retVal;
        }
        public static DateTime GetDate(object source, DateTime defaultValue)
        {
            DateTime retVal;
            try
            {
                retVal = Convert.ToDateTime(source);
            }
            catch
            {
                retVal = DateTime.Now;
            }
            return retVal == new DateTime(1, 1, 1) ? defaultValue : retVal;
        }
        public static double GetFloat(object source, float defaultValue)
        {
            double retVal;
            try
            {
                retVal = Convert.ToDouble(source);
            }
            catch
            {
                retVal = defaultValue;
            }
            return retVal;
        }
        public static double GetDouble(object obj)
        {
            double retVal;

            try
            {
                retVal = Convert.ToDouble(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }
        public static double GetDouble(object source, double defaultValue)
        {
            double retVal;

            try
            {
                retVal = Convert.ToDouble(source);
            }
            catch
            {
                retVal = defaultValue;
            }

            return retVal;
        }
        public static int GetInt(object obj)
        {
            int retVal;

            try
            {
                retVal = Convert.ToInt32(obj);
            }
            catch
            {
                retVal = 0;
            }

            return retVal;
        }
        public static bool GetBool(object source, bool defaultValue)
        {
            bool retVal;

            try
            {
                retVal = Convert.ToBoolean(source);
            }
            catch
            {
                retVal = defaultValue;
            }

            return retVal;
        }
        public static int GetInt(object source, int defaultValue)
        {
            int retVal;

            try
            {
                retVal = Convert.ToInt32(source);
            }
            catch
            {
                retVal = defaultValue;
            }

            return retVal;
        }
        public static byte GetByte(object source, byte defaultValue)
        {
            if (!(source is DBNull))
            {
                return Convert.ToByte(source);
            }
            return defaultValue;
        }
        public static string GetString(object obj)
        {
            string retVal;

            try
            {
                retVal = Convert.ToString(obj);
            }
            catch
            {
                retVal = string.Empty;
            }

            return retVal;
        }
        public static string GetString(object source, string defaultValue)
        {
            if (!(source is DBNull))
            {
                return (string)source;
            }
            return defaultValue;
        }
        public static bool GetBoolean(object source, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(source);
            }
            catch
            {
                return defaultValue;
            }
        }
        public static bool GetBoolean(object obj)
        {
            bool retVal;

            try
            {
                retVal = Convert.ToBoolean(obj);
            }
            catch
            {
                retVal = false;
            }

            return retVal;
        }
        public static List<T> MixList<T>(List<T> inputList)
        {
            try
            {
                var randomList = new List<T>();
                if (inputList.Count == 0)
                    return randomList;

                var r = new Random();
                while (inputList.Count > 0)
                {
                    var randomIndex = r.Next(0, inputList.Count);
                    randomList.Add(inputList[randomIndex]); //add it to the new, random list<>
                    inputList.RemoveAt(randomIndex); //remove to avoid duplicates
                }

                inputList.Clear();
                return randomList; //return the new random list
            }
            catch
            {
                return null;
            }
        }
        public static List<T> MixList<T>(List<T> inputList, int getCount)
        {
            try
            {
                var count = 1;
                var randomList = new List<T>();
                if (inputList.Count == 0)
                    return randomList;

                var r = new Random();
                while (inputList.Count > 0 && count <= getCount)
                {
                    count++;
                    var randomIndex = r.Next(0, inputList.Count);
                    randomList.Add(inputList[randomIndex]); //add it to the new, random list<>
                    inputList.RemoveAt(randomIndex); //remove to avoid duplicates
                }

                inputList.Clear();
                return randomList;
            }
            catch
            {
                return null;
            }
        }
        public static string Md5(string password)
        {
            if (string.IsNullOrEmpty(password)) return "";
            var textBytes = System.Text.Encoding.Default.GetBytes(password);
            var cryptHandler = new MD5CryptoServiceProvider();
            var hash = cryptHandler.ComputeHash(textBytes);
            var ret = "";
            foreach (var a in hash)
            {
                if (a < 16)
                    ret += "0" + a.ToString("x");
                else
                    ret += a.ToString("x");
            }
            return ret;
        }
        public static string AsDomain(string url)
        {
            if (string.IsNullOrEmpty(url))
                return url;

            var match = Regex.Match(url, @"^http[s]?[:/]+[^/]+");
            return match.Success ? match.Captures[0].Value : url;
        }
        public static string BuidPaging(int totalPage, int pageindex, string url)
        {
            var strPaging = "";
            if (totalPage <= 1) return string.Empty;
            if (pageindex <= 0) pageindex = 1;
            if (pageindex > totalPage) pageindex = totalPage;
            for (var i = 1; i <= totalPage; i++)
            {
                if (i == pageindex) strPaging += "<li class='active'><a href='" + string.Format(url, i) + "' title='Trang " + i + "' >" + i + "</a></li>";
                else strPaging += "<li><a href='" + string.Format(url, i) + "' title='Trang " + i + "'>" + i + "</a></li>";
                if (i == 2 && pageindex > 7)
                {
                    i = pageindex - 5;
                    strPaging += "<li><a href='javascript:void(0);' class='dot'>...</a></li>";
                }
                if ((i == pageindex + 4) && (pageindex < totalPage - 7))
                {
                    i = totalPage - 3;
                    strPaging += "<li><a href='javascript:void(0);' class='dot'>...</a></li>";
                }
            }
            const string preDis = "<li><a href='javascript:void(0);' class='disabled prev'><<</a></li>";
            var nextGo = "<li><a href='" + string.Format(url, (pageindex + 1)) + "' title='Trang " + (pageindex + 1) + "' class='next'>>></a></li>";
            const string nextDis = "<li><a href='javascript:void(0);' class='disabled next'>>></a></li>";
            var preGo = "<li><a href='" + string.Format(url, (pageindex - 1)) + "' title='Trang " + (pageindex - 1) + "' class='prev'><<</a></li>";
            if (pageindex == 1)
            {
                if (totalPage > 1) strPaging = preDis + strPaging + nextGo;
                else strPaging = preDis + strPaging + nextDis;
            }
            else if (pageindex == totalPage) strPaging = preGo + strPaging + nextDis;
            else strPaging = preGo + strPaging + nextGo;
            return "<ul class='pagination'>" + strPaging + "</ul>";
        }
        public static string GetHtmlWithFormated(string sHtml)
        {
            var sResult = sHtml.Replace("'", "\'");
            sResult = sResult.Replace(Convert.ToChar(10).ToString(CultureInfo.InvariantCulture), "");
            sResult = sResult.Replace(Convert.ToChar(13).ToString(CultureInfo.InvariantCulture), "");

            sResult = sResult.Replace(Convert.ToChar(147).ToString(CultureInfo.InvariantCulture), "\"");
            sResult = sResult.Replace(Convert.ToChar(148).ToString(CultureInfo.InvariantCulture), "\"");
            return sResult;
        }
        public static List<string> GetStrList(string sRegex, string sInputText)
        {
            var lstResult = new List<string>();

            sInputText = GetHtmlWithFormated(sInputText);
            var m = Regex.Match(sInputText, sRegex);
            while (m.Success)
            {
                lstResult.Add(m.Value);
                m = m.NextMatch();
            }

            return lstResult;
        }
        public static List<string> GetStrList(string sRegex, string sInputText, RegexOptions options)
        {
            var lstResult = new List<string>();

            sInputText = GetHtmlWithFormated(sInputText);
            var m = Regex.Match(sInputText, sRegex, options);
            while (m.Success)
            {
                lstResult.Add(m.Value);
                m = m.NextMatch();
            }

            return lstResult;
        }
        public static string ConvertDateTimeProcess(DateTime dtTime)
        {
            var dtNow = DateTime.Now;
            string rt;
            if (DateTime.Compare(dtNow, dtTime) > 0)
            {
                var ts = dtNow.Subtract(dtTime);
                if (ts.Days < 5)
                {
                    if (ts.Days > 0) rt = string.Format("Thêm vào {0} ngày {1} giờ trước", ts.Days, ts.Hours);
                    else if (ts.Hours > 0) rt = string.Format("Thêm vào {0} giờ {1} phút trước", ts.Hours, ts.Minutes);
                    else if (ts.Minutes > 0) rt = string.Format("Thêm vào {0} phút trước", ts.Minutes);
                    else if (ts.Seconds > 0)
                    {
                        rt = string.Format("Thêm vào {0} giây trước", ts.Seconds);
                    }
                    else
                    {
                        rt = "Vừa thêm vào";
                    }
                }
                else rt = ConvertDateTime04(dtTime);
            }
            else
            {
                rt = "Vừa thêm vào";
            }
            return rt;

        }
        public static string ConvertDateTime05(DateTime dtTime)
        {
            var dtNow = DateTime.Now;
            string rt;
            if (DateTime.Compare(dtNow, dtTime) > 0)
            {
                var ts = dtNow.Subtract(dtTime);
                if (ts.Days < 5)
                {
                    if (ts.Days > 0) rt = string.Format("Cập nhật {0} ngày {1} giờ trước", ts.Days, ts.Hours);
                    else if (ts.Hours > 0) rt = string.Format("Cập nhật {0} giờ {1} phút trước", ts.Hours, ts.Minutes);
                    else if (ts.Minutes > 0) rt = string.Format("Cập nhật {0} phút trước", ts.Minutes);
                    else if (ts.Seconds > 0)
                    {
                        rt = string.Format("Cập nhật {0} giây trước", ts.Seconds);
                    }
                    else
                    {
                        rt = "Vừa cập nhật";
                    }
                }
                else rt = ConvertDateTime04(dtTime);
            }
            else
            {
                rt = "Vừa cập nhật";
            }
            return rt;

        }
        public static string ConvertDateTimeFull(DateTime dtTime)
        {
            var intDay = dtTime.Day;
            var intMonth = dtTime.Month;
            var intYear = dtTime.Year;
            var strReturnValue = "";
            var dayOfWeek = "";
            switch (intMonth)
            {
                case 1: strReturnValue = ValueJanTextVn; break;
                case 2: strReturnValue = ValueFebTextVn; break;
                case 3: strReturnValue = ValueMarTextVn; break;
                case 4: strReturnValue = ValueAprTextVn; break;
                case 5: strReturnValue = ValueMayTextVn; break;
                case 6: strReturnValue = ValueJunTextVn; break;
                case 7: strReturnValue = ValueJulTextVn; break;
                case 8: strReturnValue = ValueAugTextVn; break;
                case 9: strReturnValue = ValueSepTextVn; break;
                case 10: strReturnValue = ValueOctTextVn; break;
                case 11: strReturnValue = ValueNovTextVn; break;
                case 12: strReturnValue = ValueDecTextVn; break;
            }
            switch (dtTime.DayOfWeek.ToString().ToLower())
            {
                case "monday": dayOfWeek = "Thứ hai"; break;
                case "tuesday": dayOfWeek = "Thứ ba"; break;
                case "wednesday": dayOfWeek = "Thứ tư"; break;
                case "thursday": dayOfWeek = "Thứ năm"; break;
                case "friday": dayOfWeek = "Thứ sáu"; break;
                case "saturday": dayOfWeek = "Thứ bẩy"; break;
                case "sunday": dayOfWeek = "Chủ nhật"; break;
            }
            var strResult = dayOfWeek + " Ngày " + intDay + " " + strReturnValue + ", " + intYear;
            return strResult;
        }
        public static readonly string ValueJanTextVn = "tháng 1";
        public static readonly string ValueFebTextVn = "tháng 2";
        public static readonly string ValueMarTextVn = "tháng 3";
        public static readonly string ValueAprTextVn = "tháng 4";
        public static readonly string ValueMayTextVn = "tháng 5";
        public static readonly string ValueJunTextVn = "tháng 6";
        public static readonly string ValueJulTextVn = "tháng 7";
        public static readonly string ValueAugTextVn = "tháng 8";
        public static readonly string ValueSepTextVn = "tháng 9";
        public static readonly string ValueOctTextVn = "tháng 10";
        public static readonly string ValueNovTextVn = "tháng 11";
        public static readonly string ValueDecTextVn = "tháng 12";
        public static int DefaultCatid;
        public static int DefaultPageindex = 1;
        public static int DefaultPagesize;
        public static int DefaultChoice;
        public static string BuildPage(int pagesize, int count, int currentpage, string url)
        {
            return BuidPaging(BuildTotalPage(pagesize, count), currentpage, url);
        }
        public static byte[] GenerateCapcha(string text, int width = 190, int height = 50, int fontsize = 18)
        {
            var oRandom = new Random();
            var aFontEmSizes = new[] { fontsize };
            var aFontNames = new[] { "Comic Sans MS", "Arial", "Times New Roman", "Georgia", "Verdana", "Geneva" };
            FontStyle[] aFontStyles = { FontStyle.Bold };

            var aHatchStyles = new[]

                        {
                            HatchStyle.BackwardDiagonal
                            //, HatchStyle.Cross, HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
                            //HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical, HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross,
                            //HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid, HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                            //HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard, HatchStyle.LargeConfetti, HatchStyle.LargeGrid,
                            //HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal, HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
                            //HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal, HatchStyle.NarrowVertical, HatchStyle.OutlinedDiamond,
                            //HatchStyle.Plaid, HatchStyle.Shingle, HatchStyle.SmallCheckerBoard, HatchStyle.SmallConfetti, HatchStyle.SmallGrid,
                            //HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis, HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
                            //HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
                        };

            //Creates an output Bitmap
            var oOutputBitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var oGraphics = Graphics.FromImage(oOutputBitmap);
            oGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            //Create a Drawing area
            var oRectangleF = new RectangleF(0, 0, width, height);

            //Draw background (Lighter colors RGB 100 to 255)
            Brush oBrush = new HatchBrush(aHatchStyles[oRandom.Next(aHatchStyles.Length - 1)], Color.FromArgb((oRandom.Next(100, 255)), (oRandom.Next(100, 255)), (oRandom.Next(100, 255))), Color.White);
            oGraphics.FillRectangle(oBrush, oRectangleF);

            var oMatrix = new Matrix();

            var y = height / 4;
            for (var i = 0; i <= text.Length - 1; i++)
            {
                oMatrix.Reset();
                var iChars = text.Length;
                var x = width / (iChars + 2) * i;

                //Rotate text Random
                oMatrix.RotateAt(0, new PointF(x, y));
                oGraphics.Transform = oMatrix;
                //Draw the letters with Random Font Type, Size and Color
                oGraphics.DrawString
                (
                //Text
                text.Substring(i, 1),
                //Random Font Name and Style
                new Font(aFontNames[oRandom.Next(aFontNames.Length - 1)], aFontEmSizes[oRandom.Next(aFontEmSizes.Length - 1)], aFontStyles[oRandom.Next(aFontStyles.Length - 1)]),
                //Random Color (Darker colors RGB 0 to 100)
                new SolidBrush(Color.FromArgb(0, 0, 0)), x, y
                );
                oGraphics.ResetTransform();
            }

            var oMemoryStream = new MemoryStream();
            oOutputBitmap.Save(oMemoryStream, ImageFormat.Png);
            var oBytes = oMemoryStream.GetBuffer();
            oOutputBitmap.Dispose();
            oMemoryStream.Close();
            return oBytes;
        }
    }
    public class HiResDateTime
    {
        private static long lastTimeStamp = DateTime.UtcNow.Ticks;
        public static long UtcNowTicks
        {
            get
            {
                long original, newValue;
                do
                {
                    original = lastTimeStamp;
                    var now = DateTime.UtcNow.Ticks;
                    newValue = Math.Max(now, original + 1);
                } while (Interlocked.CompareExchange(ref lastTimeStamp, newValue, original) != original);

                return newValue;
            }
        }
    }
}
