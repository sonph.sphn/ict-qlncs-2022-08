﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Constant
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Work
    {
        public const string APPROVE_AUTO_LOAD = "APPROVE_AUTO_LOAD";
        public const string WORK_AUTO_LOAD = "WORK_AUTO_LOAD";
    }
}