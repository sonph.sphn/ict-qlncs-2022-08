﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Constant
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class Template
    {
        public const string SALE_RETAIL = "SALE_RETAIL";
        public const string SALE_SENDING_TICKET = "SALE_SENDING_TICKET";
    }
}