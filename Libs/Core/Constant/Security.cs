﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Constant
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Security
    {
        public const string CURRENT_STAFF_KEY = "CURRENT_STAFF_KEY";
        public const string CURRENT_DEPARTMENT_KEY = "CURRENT_DEPARTMENT_KEY";
        public const string CURRENT_USER_KEY = "CURRENT_USER_KEY";
        public const string FUNCTIONS = "FUNCTIONS";
        public const string ACL_FUNCTIONS = "ACL_FUNCTIONS";
    }
}