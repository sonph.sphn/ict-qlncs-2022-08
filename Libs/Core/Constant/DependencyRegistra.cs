﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Constant
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public static class DependencyRegistra
    {
        public const string APPROVE_SALE_RETAIL = "APPROVE_SALE_RETAIL";
        public const string APPROVE_SALE_SERVICE = "APPROVE_SALE_SERVICE";
        public const string APPROVE_SALE_PRODUCT = "APPROVE_SALE_PRODUCT";
        public const string APPROVE_ESTATE_ORDER = "APPROVE_ESTATE_ORDER";
    }
}
