﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Constant
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Settings
    {
        public const string REQUIRE_SSL = "REQUIRE_SSL";
        public const string HOT_LINE = "HOT_LINE";
        public const string COMPANY_ADDRESS = "COMPANY_ADDRESS";
        public const string COMPANY_NAME = "COMPANY_NAME";
        public const string COMPANY_EMAIL = "COMPANY_EMAIL";
        public const string COMPANY_PHONE = "COMPANY_PHONE";
        public const string COMPANY_FAX = "COMPANY_FAX";
        public const string COMPANY_WEBSITE = "COMPANY_WEBSITE";
        public const string APP_NAME = "APP_NAME";
        public const string LOGO_URL = "LOGO_URL";
        public const string EXPORT_LOGO_URL = "EXPORT_LOGO_URL";
        public const string BASE_URL = "BASE_URL";
        public const string DATE_FORMAT = "DATE_FORMAT";
        public const string ALLOWED_EXTENSIONS = "ALLOW_FILE_EXTS";
        public const string MAX_FILE_SIZE = "MAX_FILESIZE_UPLOAD";
        public const string PBX_INBOUND_URL = "PBX_INBOUND_URL";
        public const string PBX_OUTBOUND_URL = "PBX_OUTBOUND_URL";
        public const string PBX_DB_HOST = "PBX_DB_HOST";
        public const string PBX_LOGIN_QUEUE = "PBX_LOGIN_QUEUE";
        public const string PBX_DB_NAME = "PBX_DB_NAME";
        public const string PBX_DB_PASSWORD = "PBX_DB_PASSWORD";
        public const string PBX_DB_PORT = "PBX_DB_PORT";
        public const string PBX_DB_USER = "PBX_DB_USER";
        public const string PBX_RECORDING_PATH = "PBX_RECORDING_PATH";
        public const string PBX_INBOUND_REQUEST = "PBX_INBOUND_REQUEST";
        public const string PBX_IS_PRIMARY_SERVER = "PBX_IS_PRIMARY_SERVER";
        public const string PBX_AUTO_SYNC_TIME = "PBX_AUTO_SYNC_TIME";
        public const string PBX_AUTO_SYNC_LIMIT = "PBX_AUTO_SYNC_LIMIT";
        public const string PBX_AUTO_SYNC_REMAP = "PBX_AUTO_SYNC_REMAP";
        public const string IGNORE_CUSTOMER_CREATION_FILTER = "IGNORE_CUSTOMER_CREATION_FILTER";
    }
}