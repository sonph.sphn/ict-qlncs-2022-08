﻿namespace Core.Entity.Common
{
    public class Processsing
    {
        public bool Done { get; set; }
        public float Percent { get; set; }
        public int Current { get; set; }
        public int Total { get; set; }
    }
}
