﻿namespace Core.Entity.Common
{
   public class NameId
    {
       public int Id { get; set; }
       public string Name { get; set; }
    }
}
