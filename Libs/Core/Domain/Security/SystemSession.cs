﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;
using Core.Domain.System;

namespace Core.Domain.Security
{
    [Table("System_Session")]
    public class SystemSession : BaseEntity
    {
        /// <summary>
        /// 1: From web
        /// 2: From API
        /// </summary>
        public int SourceId { get; set; }
        public int UserId { get; set; }
        [Index]
        public string Token { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime LassAccess { get; set; }
        public virtual SystemUser User { get; set; }
    }
}
