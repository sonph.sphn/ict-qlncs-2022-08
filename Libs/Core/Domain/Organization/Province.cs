﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.Organization
{
    [Table("Org_Province")]
    public class Province : BaseEntity
    {
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ParentCode { get; set; }
    }
}