﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.Organization
{
    [Table("Hnue_Career")]
    public class Career : BaseEntity
    {
        public string CareerCode { get; set; }
        public string CareerName { get; set; }
        public int CareerTHPT { get; set; }
        public int CareerOther { get; set; }
        public string CareerM1 { get; set; }
        public string CareerM2 { get; set; }
        public string CareerM3 { get; set; }
    }
}