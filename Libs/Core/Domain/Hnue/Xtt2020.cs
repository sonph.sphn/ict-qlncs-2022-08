﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.Organization
{
    [Table("Hnue_XTT2020")]
    public class Xtt2020 : BaseEntity
    {
        public string Capcha { get; set; }
        public string HoTen { get; set; }
        public string GioiTinh { get; set; }
        public string NgaySinh { get; set; }
        public string CMT { get; set; }
        public string DT { get; set; }
        public string Email { get; set; }
        public string HoKhau { get; set; }
        public string TruongC3 { get; set; }
        public string TenTruongC3 { get; set; }
        public int NamTotNghiep { get; set; }
        public string HK10K1 { get; set; }
        public string HK10K2 { get; set; }
        public string HK11K1 { get; set; }
        public string HK11K2 { get; set; }
        public string HK12K1 { get; set; }
        public string HK12K2 { get; set; }
        public string HL10 { get; set; }
        public string HL11 { get; set; }
        public string HL12 { get; set; }
        public string HTXT { get; set; }
        public string NganhDK { get; set; }
        public string MaNganh { get; set; }
        public string ToHopMon { get; set; }
        public string HSG { get; set; }
        public int HSGCapTinhNam { get; set; }
        public string HSGCapTinhMon { get; set; }
        public int HSGQGNam { get; set; }
        public string HSGQGMon { get; set; }
        public double HSGQGDiem { get; set; }
        public string TBC1 { get; set; }
        public string TBC1L10 { get; set; }
        public string TBC1L11 { get; set; }
        public string TBC1L12 { get; set; }
        public string TBC2 { get; set; }
        public double TBC2L10 { get; set; }
        public double TBC2L11 { get; set; }
        public double TBC2L12 { get; set; }
        public string TBC3 { get; set; }
        public double TBC3L10 { get; set; }
        public double TBC3L11 { get; set; }
        public double TBC3L12 { get; set; }
        public string DiemCC { get; set; }
        public double TiengAnh { get; set; }
        public double TiengPhap { get; set; }
        public double TinHoc { get; set; }
        public string NgayCapCC { get; set; }
        public bool BaiLuan { get; set; }
        public string DCBaoTin { get; set; }
        public string Khuvuc { get; set; }
        public string Doituong { get; set; }
    }
}