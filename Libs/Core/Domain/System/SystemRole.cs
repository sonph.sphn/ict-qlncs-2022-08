﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_Role")]
    public class SystemRole : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<RoleFunction> RolesFunctions { get; set; }
    }
}
