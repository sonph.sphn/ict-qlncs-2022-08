﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_UserRole")]
    public class UserRole : BaseEntity
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public virtual SystemUser User { get; set; }
        public virtual SystemRole Role { get; set; }
    }
}