using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;
using Core.Base;
using Newtonsoft.Json;

namespace Core.Domain.System
{
    [Table("System_User")]
    public class SystemUser : BaseEntity
    {
        public double Salary { get; set; }
        public double Allowance { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public string Avatar { get; set; }
        public bool Locked { get; set; }
        public bool Limited { get; set; }
        public string Token { get; set; }
        public DateTime TokenOn { get; set; } = DateTime.Now;
        public DateTime LastAccess { get; set; } = DateTime.Now;
        [JsonIgnore]
        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual ICollection<UserRole> UsersRoles { get; set; }
    }
}