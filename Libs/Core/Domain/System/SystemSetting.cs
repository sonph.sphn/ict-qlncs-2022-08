﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_Setting")]
    public class SystemSetting : BaseEntity
    {
        public int GroupId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Desc { get; set; }
        public virtual SystemSettingGroup Group { get; set; }
    }
}