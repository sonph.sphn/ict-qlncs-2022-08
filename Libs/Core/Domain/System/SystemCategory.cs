﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_Category")]
    public class SystemCategory : BaseEntity
    {
        public int Order { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
    }
}