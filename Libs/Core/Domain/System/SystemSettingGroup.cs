﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_SettingGroup")]
    public class SystemSettingGroup : BaseEntity
    {
        public string Name { get; set; }
    }
}