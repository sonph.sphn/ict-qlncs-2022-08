﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.Base;

namespace Core.Domain.System
{
    [Table("System_RoleFunction")]
    public class RoleFunction : BaseEntity
    {
        public int RoleId { get; set; }
        public int FunctionId { get; set; }
        public virtual SystemFunction Function { get; set; }
        public virtual SystemRole Role { get; set; }
    }
}