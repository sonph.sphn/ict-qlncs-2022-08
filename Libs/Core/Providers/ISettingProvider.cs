﻿namespace Core.Providers
{
    public interface ISettingProvider
    {
        string GetValue(string key, string defaultValue = "");
    }
}