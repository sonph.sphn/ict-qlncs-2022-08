﻿using System.Diagnostics.CodeAnalysis;

namespace Core.Model.Common
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ResultData
    {
        public ResultData()
        {

        }

        public ResultData(bool success, object data, string message = "")
        {
            this.success = success;
            this.message = message;
            this.data = data;
        }
        public ResultData(bool success, string message, object data = null)
        {
            this.success = success;
            this.message = message;
            this.data = data;
        }
        public bool success { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

    public static class Helper
    {
        public static ResultData ToResponseResult(this object data, bool success = true, string message = "")
        {
            return new ResultData
            {
                data = data,
                message = message,
                success = success
            };
        }
    }
}