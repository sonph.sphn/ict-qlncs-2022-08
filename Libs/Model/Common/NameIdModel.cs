﻿namespace Core.Model.Common
{
    public class NameIdModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
