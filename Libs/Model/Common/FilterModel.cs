﻿namespace Core.Model.Common
{
    public class FilterModel
    {
        public int ParentId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}