﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Domain.System;

namespace Core.Model.Security
{
    [NotMapped]
    public class SettingGroupModel : SystemSettingGroup
    {
        [Required]
        public new string Name { get; set; }
    }
}
