﻿using System.ComponentModel.DataAnnotations;

namespace Core.Model.Security
{
    public class RoleModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}