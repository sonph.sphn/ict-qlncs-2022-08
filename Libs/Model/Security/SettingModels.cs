﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Domain.System;

namespace Core.Model.Security
{
    [NotMapped]
    public class SettingModel : SystemSetting
    {
        [Required]
        public new string Key { get; set; }
    }
}