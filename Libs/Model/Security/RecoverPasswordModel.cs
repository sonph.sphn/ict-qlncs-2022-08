﻿using System.ComponentModel.DataAnnotations;

namespace Core.Model.Security
{
    public class RecoverPasswordModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare(nameof(Password))]
        public string Confirm { get; set; }
    }
}
