﻿using System.ComponentModel.DataAnnotations;

namespace Core.Model.Security
{
    public class PasswordModel
    {
        public int Id { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare(nameof(Password))]
        public string Confirm { get; set; }
    }
}