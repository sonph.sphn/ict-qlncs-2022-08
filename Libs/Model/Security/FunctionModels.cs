﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Core.Domain.System;

namespace Core.Model.Security
{
    [NotMapped]
    public class FunctionModel : SystemFunction
    {
        [Required]
        public new int Order { get; set; }
        [Required]
        public new string Key { get; set; }
        [Required]
        public new string Name { get; set; }
        [Required]
        public new string Url { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }

    public class FunctionCategoryModel
    {
        public int Id { get; set; }
        public int Order { get; set; }
        [Required]
        public string Icon { get; set; }
        [Required]
        public string Name { get; set; }
    }
}