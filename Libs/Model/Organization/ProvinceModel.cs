﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Core.Model.Organization
{
    public class ProvinceModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public IEnumerable<SelectListItem> Provinces { get; set; }
    }
}