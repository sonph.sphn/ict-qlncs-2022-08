﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Domain.System;

namespace Core.Model.User
{
    [NotMapped]
    public class UserModel : SystemUser
    {
        [EmailAddress]
        [Required]
        public new string Email { get; set; }
        [Required]
        public new string Name { get; set; }
        public new string Password { get; set; }
        [Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }
}