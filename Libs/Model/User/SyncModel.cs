﻿using System.ComponentModel.DataAnnotations;

namespace Core.Model.User
{
    public class SyncModel
    {
        public int Id { get; set; }
        [Required]
        [Compare("ConfirmPassword")]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public bool SendEmail { get; set; }
    }
}